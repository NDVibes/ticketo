<?php
    function menu() {
        $str = '
        <div id="menu-btn">
            <img id="menu-btn-image" src="../images/menu.png" />
        </div>
        <div id="menu">
            <a href="javascript:void(0);" class="close-btn">&times;</a>
            
            <div id="top-controls">
                <a href="index.php" title="Go to homepage.">
                    <img src="../images/adminPanelImages/home.png" />
                </a>
                <a href="logout.php" title="Logout.">
                    <img src="../images/adminPanelImages/logout.png" />
                </a>
            </div>

            <div id="menu-content">';

            $str .= file_get_contents(getcwd() . "/controls.html");
            $str .= '

            </div>
        </div>

        ';

        echo $str;
    }
