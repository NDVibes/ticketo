<?php
    include_once('../php/config.php');
    include_once('../php/Auth.php');
    
    session_start();
    
    //Authenticate
    if ($_SESSION['auth']->user != 'scan') unset($_SESSION['auth']); 
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = '../admin/auth.php';
    $auth->ret = '../scan/index.php';
    $auth->user = 'scan';
    $_SESSION['auth'] = $auth;
    $auth->prompt();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Scanner</title>
        
        <style>
            body {
                font-family: Arial;
                background-color: #424242;
            }
            #webcodecam-canvas {
                width: 100%;
                height: 75vh;
                display: none;
            }
            #start-btn {
                width: 100%;
                height: 80vh;
                background-color: #000000;
                color: #FF6600;
                font-weight: bold;
                margin-top: 20px;
                position: relative;
            }
            #start-btn span {
                text-align: center;
                width: 100%;
                font-size: 80px;
                font-weight: bold;
                margin: auto;
                position: absolute;
                top: 50%;
                transform: translate(0, -50%);
            }
            #camera-select {
                width: 100%;
                background-color: #000000;
                color: #FF6600;
                padding: 10px;
                font-size: 30px;
            }
            
            #result {
                width: 100%;
                padding-left: 50px;
                padding-right: 50px;
                box-sizing: border-box;
                font-size: 50px;
                text-align: center;
                margin-top: 50px;
                color: #FFFFFF;
            }
            #result .ID {
                font-weight: bold;
            }
            #result-img {
                width: 150px;
                height: 150px;
                margin: auto;
                margin-top: 50px;
                display: none;
            }
        </style>
    </head>
    <body>
        
        <canvas id="webcodecam-canvas"></canvas>
        <select id="camera-select"></select>
        
        <div id="start-btn" onclick="start()">
            <span>Start</span>
        </div>
        
        <div id="result"></div>
        <img id="result-img" src="media/images/image-placeholder.png" />
        
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/qrcodelib.js"></script>
        <script type="text/javascript" src="js/webcodecamjs.js"></script>
        <script type="text/javascript">
            
            //Variable to hold variable of prevously scanned QRCode to prevent double-scans
            prev = "";
            
            //Defining all the arguments for the scanner
            var args = {
            
                //Function launched when QR-Code is scanned
                resultFunction: function(result) {
                    if (result.code != prev) {
                            
                        //Split ID and hash
                        var parts = result.code.split(',');
                        
                        //Filter ID to prevent XSS
                        if (!isInt(parts[0])) {
                            return;
                        }
                        
                        //Sending AJAX request to server to check if hash is valid  
                        $.get( "<?php echo htmlspecialchars(HOST) ?>/ticketvalidation.php", {id: parts[0],hash: parts[1]} , function( response ) {  
                        
                                if (response == "true") {
                                    $("#result").html("<span style='color:#00FF00;' class='ID'>" + parts[0] + "</span> is toegelaten...");
                                    $("#result-img").attr("src", "media/images/accepted.png");
                                    window.navigator.vibrate([200]);
                                } else {
                                    $("#result").html("<span style='color:#FF0000;' class='ID'>" + parts[0] + "</span> is geweigerd...");
                                    $("#result-img").attr("src", "media/images/declined.png");
                                    window.navigator.vibrate([1200]);
                                }
                                    
                        });
                    }
                    
                    prev = result.code;
                },
                codeRepetition: false,
                tryVertical: true,
                constraints: {                          
                    video: {
                        mandatory: {
                        // maxWidth: 1280,
                            //maxHeight: 720
                        }
                    },
                    audio: true
                },
                zoom: 3,
                contrast: 10,
                grayscale: true,
                successTimeout: 0,
                beep: "media/audio/beep.mp3"
            };
            
            
            //Make WebCodeCamJS-object
            decoder = new WebCodeCamJS("#webcodecam-canvas").buildSelectMenu("#camera-select", "environment|back").init(args);
            
            //Function to start the camera and decorder on-click
            function start() {            
                //Change UI
                $("#webcodecam-canvas").fadeIn();
                $("#camera-select").fadeOut();
                $("#start-btn").fadeOut();
                $("#result-img").fadeIn();
                $("#result-img").css('display', 'block');
                
                //Start decoder
                if (decoder.play())
                    document.cancelFullscreen();
            }
            
            //Function to check if a given value is an integer
            //:param value: Value to check for integer
            //:return bool: Returns true if int
            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }
        </script>
    </body>
</html>
