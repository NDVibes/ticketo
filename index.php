<?php
    require_once('php/index_page_script.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <?php readfile(getcwd() . "/partials/head.html") ?>
        <link rel="stylesheet" type="text/css" href="style/index.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <div id="content">
                <div class="container">
                    <?php
                        //Header
                        readfile(getcwd() . "/partials/header.html");
                    ?>


                    <div class="form-wrapper">
                        <div id="option-select">
                            <div class="option">
                                <span id="one">Bestel enkel ticket</span>
                            </div>
                            <div class="option">
                                <span id="multiple">Bestel meerdere ticketen</span>
                            </div>
                        </div>
                        <form method="post" action="index.php">
                            <input type="text" name="firstname[]" placeholder="Voornaam" id="focus" value="<?php if(isset($_SESSION['temp_users'][0]->firstname)) echo $_SESSION['temp_users'][0]->firstname; ?>" required />
                            <input type="text" name="lastname[]" placeholder="Achternaam" value="<?php if(isset($_SESSION['temp_users'][0]->lastname)) echo $_SESSION['temp_users'][0]->lastname; ?>" required />
                            <br>
                            <input type="email" name="email[]" placeholder="voorbeeld@mail.com" value="<?php if(isset($_SESSION['temp_users'][0]->email)) echo $_SESSION['temp_users'][0]->email; ?>" required />
                            <br>
                            <label>Geboortedatum</label>
                            <input type="date" id="date" name="birthdate[]" value="<?php if (isset($_SESSION['temp_users'][0]->birthdate)) echo $_SESSION['temp_users'][0]->birthdate; ?>" placeholder="mm/dd/yyyy" required onfocusout="displayMinSixteen(this.value)" />
                            <br>
                            <label>Kortingscode</label>
                            <input type="text" name="coupon" placeholder="00000" />
                            <br class="mobile">
                            <input type="checkbox" name="accepted1[]" value="1" required />
                            <span class="tos"><a target="_blank" href="algemene-voorwaarden.html">Algemene Voorwaarden</a></span>
                            <br>
                            <input type="checkbox" name="accepted2[]" value="1" required />
                            <span class="tos"><a target="_blank" href="privacyverklaring.html">Privacyverklaring</a></span>
                            <br>

                            <div id="min16">
                                <input type="checkbox" name="accepted3[]" value="1" required checked />
                                <span class="tos">Ik ben -16, maar heb toestemming van mijn ouders</span>
                            </div>
                    
                            <hr id="hrTicket">
                            
                            <?php
                                if (isset($_SESSION['temp_users']) && sizeof($_SESSION['temp_users']) > 1) {
                                    $loopOne = false;
                                    foreach ($_SESSION['temp_users'] as $temp_user) {
                                        if (!$loopOne) {
                                            $loopOne = true;
                                            continue;
                                        }

                                        echo '
                                            <div class="ticket">
                                                <div class="close">x</div>
                                                <input type="text" name="firstname[]" placeholder="Voornaam" id="focus" value="'.htmlspecialchars($temp_user->firstname).'" required />
                                                <input type="text" name="lastname[]" placeholder="Achternaam" value="'.htmlspecialchars($temp_user->lastname).'" required />
                                                <input type="email" name="email[]" placeholder="voorbeeld@mail.com" value="'.htmlspecialchars($temp_user->email).'" required />
                                                <br>
                                                <label>Geboortedatum</label>
                                                <input type="date" name="birthdate[]" value="'.htmlspecialchars($temp_user->birthdate).'" required />
                                                <br>

                                            </div>
                                        
                                        ';
                                    }
                                }
                            ?>
                            <div id="ticket-container">
                            </div>
                            <div id="add-ticket">
                                <span>Voeg ticket toe</span>
                            </div>


                            <br class="mobile">
                            <br class="mobile">
                            <input type="submit" value="Volgende" name="btnNext" />
                            <br class="mobile">


                            <script>
                                showProgress(1,5);
                            </script>
                        </form>

                    </div>
                    <div class="price">
                        <span>Prijs: &euro; <?php echo htmlspecialchars($PRICE); ?></span>
                    </div>
                    <div class="dymanic-btn">
                        <a id="koopNu-btn" class="a-btn">KOOP NU JOUW TICKET</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
