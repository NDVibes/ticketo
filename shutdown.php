<?php
    require_once('php/shutdown_page_script.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <?php readfile(getcwd() . "/partials/head.html") ?>
        <link rel="stylesheet" type="text/css" href="style/check.css">
    </head>
    <body>
        <div id="wrapper">
            <div id="content">
                <div class="container">
                    <?php
                        //Header
                        readfile(getcwd() . "/partials/header.html");
                    ?>

                    <div class="form-wrapper">
                        <div class="form-text">
                            Gesloten<br>
                            <span>Onze online voorverkoop is gesloten.</span>
                            <br><br>
                            <a href="../index.html>Home</a>
                        </div>
                        <script>
                            showProgress(0,5);
                        </script>
                    </div>
                </div>
            </div>
            <?php
                
                        readfile(getcwd() . "/partials/footer.html");
                    ?>
        </div>
    </body>
</html>
