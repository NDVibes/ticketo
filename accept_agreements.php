<?php
    require_once('php/accept_agreements_page_script.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <?php readfile(getcwd() . "/partials/head.html") ?>
        <script src="js/index.js"></script>
        <link rel="stylesheet" type="text/css" href="style/index.css">

        <style>
            .form-wrapper { display: block; }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="content">
                <div class="container">
                    <?php
                        //Header
                        readfile(getcwd() . "/partials/header.html");
                    ?>

                    <div class="form-wrapper">
                        <div class="form-text">
                            
                            Aanvaard nog even onze voorwaarden.
                        </div>

                        <form method="POST" action="">
                            <input type="checkbox" name="accepted1" value="1" required />
                            <span class="tos"><a target="_blank" href="algemene-voorwaarden.html">Algemene Voorwaarden</a></span>
                            <br>
                            <input type="checkbox" name="accepted2" value="1" required />
                            <span class="tos"><a target="_blank" href="privacyverklaring.html">Privacyverklaring</a></span>
                            <br>

                            <div id="min16">
                                <input type="checkbox" name="accepted3" value="1" required checked />
                                <span class="tos">Ik ben -16, maar heb toestemming van mijn ouders</span>
                            </div>

                            <br><br>
                            <input type="submit" name="btnAccept" value="Volgende" />
                        </form>
                        <script>
                            showProgress(4,5);
                        </script>
                    </div>
                </div>
            </div>
            <?php
                
                        readfile(getcwd() . "/partials/footer.html");
                    ?>
        </div>
    </body>
</html>
