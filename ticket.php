<?php
    require_once('php/ticket_page_script.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <?php readfile(getcwd() . "/partials/head.html") ?>
        <link rel="stylesheet" type="text/css" href="style/ticket.css">
    </head>
    <body>
        <iframe width="1" height="1" frameborder="0" style="visibility: hidden;" src="print.php"></iframe>
    
        <div id="wrapper">
            <div id="content">
                <div class="container">
                    <?php
                        //Header
                        readfile(getcwd() . "/partials/header.html");
                    ?>

                    <div class="form-wrapper">
                        <div class="form-text">
                            Uw ticket<br>
                            <span>Druk uw ticket nu af!</span>
                            
                            <div class="ticket">
                                <img class="logo" src="images/BalTropical_logo_small.png" />
                                <div class="logo-text">Online Voorverkoop</div>
                                <img class="palmboom" src="images/palmboom.png" />
                                <div class="ndvibes left">
                                    NDVibes
                                </div>
                                <div class="ndvibes right">
                                    Niel Duysters
                                </div>
                                <div class="user-info">
                                    <span class="firstname"><?php echo htmlspecialchars($order->users[0]->firstname); ?></span>
                                    <span class="lastname"><?php echo htmlspecialchars($order->users[0]->lastname); ?></span>
                                    <br>
                                    
                                    <span class="birthdate"><?php echo htmlspecialchars($order->users[0]->birthdate); ?></span>
                                    <span class="age">(<?php echo htmlspecialchars($order->users[0]->getAge()); ?>)</span>
                                    <br>
                                    
                                    <span class="code"><?php echo htmlspecialchars($pseudocode); ?></span>
                                </div>
                                
                                <img class="qrcode" src="<?php echo htmlspecialchars(HOST."/".$path); ?>" />
                            </div>
                            
                            <br><br>
                            <a href="print.php" target="_blank">Afdrukken</a>
                            <a href="bedankt.php">Volgende</a>
                        </div>
                        <script>
                            window.onload = function() {
                                if (screen.width < 801) {
                                    window.location.replace("screenshot-ticket.php");
                                }
                            }

                            showProgress(4,5);
                        </script>
                    </div>
                </div>
            </div>
            <?php
                readfile(getcwd() . "/partials/footer.html");
            ?>
        </div>
    </body>
</html>
