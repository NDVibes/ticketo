$(document).ready(function() {
    document.getElementById("menu-btn").addEventListener("click", openMenu);
    document.getElementsByClassName("close-btn")[0].addEventListener("click", closeMenu);

    function openMenu() {
        var width = "70%";
        
        $("#menu").css("width", width);
        $("#menu-btn").css("visibility", "hidden"); 
    }

    function closeMenu() {
        $("#menu").css("width", "0");
        $("#menu-btn").css("visibility", "visible"); 
    }

    //Filling menu with controls
});
