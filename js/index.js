$(document).ready(function() {
    $("#koopNu-btn").click(function() {     
        //Form slide effect
        $(".form-wrapper").slideDown(1000);

        //Make price fade-out
        $(".price span").fadeOut(500);
        
        //Set new margin
        $(this).css({'margin-top': '7px'});
    
        //Display progress
        $(".progress").fadeIn(1300);
    });
    
    displayMinSixteen($("#date").val());

    $("#add-ticket").click(function() {
        let html = `
            <div class="ticket">
                <a class="close"><span>&times;</span></a>
                <input type="text" name="firstname[]" placeholder="Voornaam" id="focus" required />
                <input type="text" name="lastname[]" placeholder="Achternaam" required />
                <input type="email" name="email[]" placeholder="voorbeeld@mail.com" required />
                <br>
                <label>Geboortedatum</label>
                <input type="date" id="date" name="birthdate[]" placeholder="mm/dd/yyyy" required />
                <br>

            </div>
        `;

        $("#ticket-container").append(html);
    });

    $(document).on('click', '.ticket .close', function() {
        console.log("x");
        $(this).parent().fadeOut(300, function() {
            $(this).remove();
        });
    });

    $(".option span").click(function() {
        if (this.id == "one") {
            this.style.color = "#FF6600";
            $("#multiple").css("color", "#615760");

            $("#add-ticket").fadeOut(500);
            $("#ticket-container").fadeOut(500, function() {
                $(this).html("");
            });
            $("#hrTicket").fadeOut(500);
        } else {
            this.style.color = "#FF6600";
            $("#one").css("color", "#615760");
            
            $("#add-ticket").fadeIn(500);
            $("#ticket-container").fadeIn(500);
            $("#hrTicket").fadeIn(500);
        }
    });
});


function displayMinSixteen(date) {
    if (date == undefined)
        return;

    if (Date.parse(date)) {
        date = new Date(date);
        if (calculateAge(date) < 16) {
            $("#min16 input").prop('checked', false);
            $("#min16").fadeIn();
        } else {
            $("#min16 input").prop('checked', true);
            $("#min16").fadeOut();
        }
    }
}
function calculateAge(birthday) {
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}
