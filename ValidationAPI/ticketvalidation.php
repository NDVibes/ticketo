<?php
    ini_set('display_errors, 1');

    //Controller to check if scanned ticket is valid
    include_once('php/Database.php');
    
    //Getting info
    $id = $_GET['id'];
    $hash = $_GET['hash'];
    
    //Check if values are valid
    if (!(isset($id) && isset($hash))) {
        echo 'false';
        return false;
    }
    
    //Database connection
    $db = new Database();
    
    //Get paymentID
    $paymentID = $db->selectById($id, 'PaymentID');
    //Get database hash
    $db_hash = $db->select($paymentID, 'hash');
    
    //Check if values are valid
    if (!(isset($paymentID) && isset($db_hash))) {
        echo 'false';
        return false;
    }
    
    //Check if this user has paid
    if (!$db->select($paymentID, 'paid') == 1) {
        echo 'false';
        return false;
    }
    
    //Check if already scanned
    if ($db->select($paymentID, 'scanned') == 1) {
        echo 'false';
        return false;
    }
    
    //Check if db_hash matches hash on ticket
    if ($db_hash != $hash) {
        echo 'false';
        return false;
    }
    
    //Validated!
    //Make scanned true
    if ($db->update($paymentID, array(
        'scanned' => 1
    ))) {
        //Return true
        echo 'true';
        return true;
    }
    
    
