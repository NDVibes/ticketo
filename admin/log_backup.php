<?php

    include_once('../php/config.php');
    include_once('../php/Database.php');
    include_once('../php/Auth.php');
    session_start();
    
    ini_set('display_errors', DEBUG);
    
    //Authenticate
    if ($_SESSION['auth']->user != 'admin') unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'log.php';
    $auth->user = 'admin';
    $_SESSION['auth'] = $auth;
    $auth->prompt();
?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    body {
        background-color: #111111;
        color: #FFFFFF;
        font-family: Arial;
    }
    .failure {
        color: #FF0000;
    }
    .success {
        color: #00FF00;
    }
    .important {
        color: #0000FF;
        font-weight: bold;
    }
    
</style>

<h2>Logfile</h2>

<!----LOGS BEGIN HERE ---------> 
