<?php
    include_once('../config/config.php');
    include_once(PATH . 'php/classes/Auth.php');
    include_once(PATH . '/php/classes/Log.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 2;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'giveaway.php';
    $auth->neededPermission = $NEEDED_PERMISSION;
    $_SESSION['auth'] = $auth;
    $auth->prompt();
?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
		

        <style>
            body {
                background-color: #000000;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            h1 {
                padding-bottom: 0;
                margin-bottom: 5px;
            }
            #container {
                width: 1150px;
                margin: auto;
            }
            #header-container {
                min-height: 150px;
                max-height: 250px;
                background-color: #6E6E6E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            table, tr, td, th {
                border-collapse: collapse;
                border: solid 1px #002B02;
                padding: 10px;
            }
            table {
                margin: auto;
                width: 100%;
            }
            #btnAddRow {
                background-color: transparent;
                border: 0;
                color: #6E6E6E;
                padding: 0;
                margin: 0;
                margin-top: 5px;
                cursor: pointer;
                font-weight: bold;
                font-size: 0.90em;
            }

            td input {
                background-color: #111111;
                border: 0;
                color: #FFFFFF;
                font-weight: bold;
                padding: 5px;
                width: 100%;
                box-sizing: border-box;
                font-size: 1em;
                caret-color: #00FF00;
            }
            #btnAddUsers {
                margin-top: 10px;
                float: right;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="container">
            <div id="header-container">


            </div>
            <h1>Add user</h1>
            <p>
                Manually add users to the database. Added users will receive their ticket by mail without needing to pay
                or accept agreements.
            </p>
            <div id="option-container">
                <div id="option1"></div>
                <div id="option2"></div>
                <div id="option3">
                    <h2>Add user manually</h2>
                    <form method="post" name="" action="../php/adminPanelCode/addUserHandler.php" id="tableForm">
                        <input type="hidden" value="" id="auth" name="auth" />
                    <table id="add-user">
                        <thead>
                            <tr>
                                <td>Firstname</td>
                                <td>Lastname</td>
                                <td>E-mail</td>
                                <td>Birthdate</td>
                            </tr>
                        </thead>
                            <tbody id="tableBody">
                                    <tr>
                                        <td>
                                            <input type="text" name="firstname[]" class="input" placeholder="Firstname" required />
                                        </td>
                                        <td>
                                            <input type="text" name="lastname[]" class="input" placeholder="Lastname" required />
                                        </td>
                                        <td>
                                            <input type="email" name="email[]" class="input" placeholder="example@email.com" required />
                                        </td>
                                        <td>
                                            <input type="date" name="birthdate[]" class="input" required />
                                        </td>
                                    </tr>
                            </tbody>
                            
                    </table>
                    <input type="submit" value="Add all users in table" name="btnAddUsers" id="btnAddUsers" />
                     </form>
                    <button id="btnAddRow">+ Add row</button>
                </div>
            </div>
        </div>
        <script>
            window.onload = function() {
                document.getElementById("auth").value = getCookie("AJAX_AUTH");
            }

            document.getElementById("btnAddRow").addEventListener("click", addRow);

            function addRow() {
                html = `
                    <tr>
                        <td>
                            <input type="text" name="firstname[]" class="input" placeholder="Firstname" />
                        </td>
                        <td>
                            <input type="text" name="lastname[]" class="input" placeholder="Lastname" />
                        </td>
                        <td>
                            <input type="email" name="email[]" class="input" placeholder="example@email.com" />
                        </td>
                        <td>
                            <input type="date" name="birthdate[]" class="input" />
                        </td>
                    </tr>
                `;

                $("#tableBody").append(html);
            }
            
            function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
        </script>
    </body>
</html>

