<?php
    include_once("../config/config.php");
    include_once("../php/classes/Log.php");
    include_once("../php/classes/Auth.php");
    session_start();

    ini_set('display_errors', 1);

    if (!isset($_SESSION['auth']))
        header('Location: index.php');

    //Delete auth-string from auth-file
    $auth_str = $_SESSION['auth']->auth_str;
    $file = file_get_contents("AUTH_COOKIES/authcookie.txt.php");

    $content = explode(':', $file);
    $newStr = "";
    foreach ($content as $c) {
        if ($c != $auth_str)
            $newStr .= $c . ":";
    }
    $newStr = substr($newStr, 0, -1);

    $f = fopen("AUTH_COOKIES/authcookie.txt.php", 'w');
    fwrite($f, $newStr);
    fclose($f);

    $adminlog = new AdminLog();
    $adminlog->write($_SESSION['auth']->user, "Logged out.");

    unset($_SESSION['auth']);

    header('Location: index.php');
    exit(1);
    return;
