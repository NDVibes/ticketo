<?php

    include_once('../config/config.php');
    include_once('../php/classes/Auth.php');

    ini_set('display_errors', 1);
    session_start();    

    $auth = $_SESSION['auth'];
        
    if (isset($_POST['btnLogin'])) {
        $auth->check($_POST['username'], $_POST['password']);
    }
?>

<!DOCTYPE html>
<html>
    <head>
    
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <style>
            #login {
                padding: 55px;
                background-color: #FFFFFF;
                color: #000000;
                border: solid 2px #000000;
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
            }
            #login input {
                padding: 4px;
                margin-top: 5px;
                border: solid 1px #A4A4A4;
            }
            #login input[type="submit"] {
                background-color: #5E69FF;
                color: #FFFFFF;
                font-weight: bold;
                padding: 5px;
            }  
        </style>
    </head>
    <body>
        <form id="login" method="post" name="" action="">
            <b>Admin</b><br>
            <input name="username" type="text" /><br>
            <input name="password" type="password" /><br>
            <input name="btnLogin" value="Login" type="submit" />
        </form> 
    </body>
</html>
