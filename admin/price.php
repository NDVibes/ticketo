<?php
    include_once('../config/config.php');
    include_once(PATH . '/php/classes/Auth.php');
    include_once(PATH . '/php/classes/Log.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 2;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'price.php';
    $auth->permission = 2;
    $_SESSION['auth'] = $auth;
    $auth->prompt();

    $nielson = new Nielson(PATH . "/config/systemconfig.nielson.php");
    $nielson->read();

?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
		

        <style>
            body {
                background-color: #1C1C1C;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            h1 {
                padding-bottom: 0;
                margin-bottom: 5px;
            }
            #container {
                min-height: 150px;
                max-height: 250px;
                background-color: #2E2E2E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            #search-container,
            #settings-container {
                width: 50%;
                float: left;
            }

            #search-container .title,
            #settings-container .title {
                font-size: 120%;
                font-weight: bold;
            }
            #main {
                max-width: 1050px;
                margin: auto;
                padding-bottom: 150px;
            }

            .section form span {
                display: block;
            }
            .section form input[type="text"] {
                width: 250px;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="main">
            <div id="container">
                <div id="settings-container">
                    <span class="title">Price View Settings</span><br><br>
                </div>
            </div>

            <h1>Price Settings</h1>
            <br><br>
            <div id="config-container">
                <div class="section">
                    <form method="post" action="../php/adminPanelCode/configHandler.php">
                        <input type="hidden" value="" class="auth" name="auth" />
                        <h2>Set price</h2>
                        <span>Set price:</span>
                        <input type="number" name="price" value="<?php echo $nielson->getObject('setting', 'price')->getProperty("value")->value; ?>" /><br><br>
                        <input type="submit" name="btnPrice" value="Set price" />
                    </form>
                </div>
            </div>
        </div>

        <script>
            window.onload = function() {
                $(".auth").each(function() {
                    $(this).val(getCookie("AJAX_AUTH"));
                });
            }


            function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
        </script>
    </body>
</html>

