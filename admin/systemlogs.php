<?php
    include_once('../config/config.php');
    include_once(PATH . '/php/classes/Auth.php');
    include_once(PATH . '/php/classes/Log.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 1;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'systemlogs.php';
    $auth->permission = 1;
    $_SESSION['auth'] = $auth;
    $auth->prompt();
?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
		

        <style>
            body {
                background-color: #1C1C1C;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            h1 {
                padding-bottom: 0;
                margin-bottom: 5px;
            }
            #container {
                min-height: 150px;
                max-height: 250px;
                background-color: #2E2E2E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            #search-container,
            #settings-container {
                width: 50%;
                float: left;
            }

            #search-container .title,
            #settings-container .title {
                font-size: 120%;
                font-weight: bold;
            }
            .filter {
                margin-top: 5px;
            }
            .filter select {
                background-color: #1C1C1C;
                color: #FF6600;
                padding: 6px;
                border: 0;
                width: 125px;
            }
            .filter select option {
                background-color: #1C1C1C !important;
                color: #FF6600;
                padding: 6px;
                border: 0;
                width: 125px;
            }
            .filter .operator {
                padding: 6px;
                width: 30px;
                margin: 0;
                background-color: #1C1C1C;
                color: #FF6600;
                border: 0;
                text-align: center;
                cursor: pointer;
            }
            .filter #filterValue {
                padding: 6px;
                border: 0;
                background-color: #1C1C1C;
                color: #FF6600;
                font-weight: bold;
            }

            #addFilter {
                background-color: transparent;
                border: 0;
                color: #848484;
                padding: 0;
                margin: 0;
                cursor: pointer;
                font-weight: bold;
            }
            #settings-wrapper * {
                margin-bottom: 10px;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="container">
            <div id="settings-container">
                <span class="title">System Log View Settings</span><br><br>
                <div id="settings-wrapper">
                    <button id="refresh" title="Manually refresh the logs." onclick="window.location.reload();">Refresh</button><br>
                    <a href="#phplogs">
                        <button>View PHP Errors</button>
                    </a><br>
                    <a href="#systemlogs">
                        <button>View System Messages</button>
                    </a>
                </div>
            </div>
        </div>

        <h1>System Logs</h1>
        <br><br>
        <div id="logs">
            <div id="phplogs">
                <h2>PHP Errors</h2>
                <?php
                    $file = array_reverse(file("systemlogs/phplog.log.php"));
                    
                    $skip = false;
                    foreach ($file as $line) {
                        if ($skip)
                            echo nl2br( $line );
                        
                        $skip = true;
                    }

                    echo "</div><br><br><hr><br><div id='systemlogs'><h2>System Messages</h2>";
                    
                    $log = new SystemLog("systemlogs/systemlog.nielson.php");
                    echo (string)$log;
                ?>
            </div>
        </div>
    </body>
</html>

