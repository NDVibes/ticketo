<?php
    include_once('../config/config.php');
    include_once('../php/classes/Auth.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 1;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'logs.php';
    $auth->permission = 1;
    $_SESSION['auth'] = $auth;
    $auth->prompt();
    
    
?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
		

        <style>
            body {
                background-color: #1C1C1C;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            h1 {
                padding-bottom: 0;
                margin-bottom: 5px;
            }
            #container {
                min-height: 150px;
                max-height: 250px;
                background-color: #2E2E2E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            #search-container,
            #settings-container {
                width: 50%;
                float: left;
            }

            #search-container .title,
            #settings-container .title {
                font-size: 120%;
                font-weight: bold;
            }
            .filter {
                margin-top: 5px;
            }
            .filter select {
                background-color: #1C1C1C;
                color: #FF6600;
                padding: 6px;
                border: 0;
                width: 125px;
            }
            .filter select option {
                background-color: #1C1C1C !important;
                color: #FF6600;
                padding: 6px;
                border: 0;
                width: 125px;
            }
            .filter .operator {
                padding: 6px;
                width: 30px;
                margin: 0;
                background-color: #1C1C1C;
                color: #FF6600;
                border: 0;
                text-align: center;
                cursor: pointer;
            }
            .filter #filterValue {
                padding: 6px;
                border: 0;
                background-color: #1C1C1C;
                color: #FF6600;
                font-weight: bold;
            }

            #addFilter {
                background-color: transparent;
                border: 0;
                color: #848484;
                padding: 0;
                margin: 0;
                cursor: pointer;
                font-weight: bold;
            }
            #settings-wrapper * {
                margin-bottom: 10px;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="container">
            <div id="search-container">
                <span class="title">Search logs</span><br>
                <span>Display logs where...</span><br>
                <div id="filter-wrapper">
                    <div class="filter">
                        <select id="column" name="column" class="updateOnChange">
                            <option value='userID'>User ID</option>
                            <option value='PaymentID'>Payment ID</option>
                            <option value='username'>Username</option>
                            <option value='email'>E-mail</option>
                            <option value='datetime'>Date</option>
                        </select>

                        <input type="text" data-value="0" value="=" class="operator" name="operator" readonly />

                        <input type="text" name="filterValue" placeholder="Value" id="filterValue" class="updateOnChange" />
                    </div>
                </div>
                <br><button id="addFilter">+ Add filter</button>
            </div>
            <div id="settings-container">
                <span class="title">Log View Settings</span><br><br>
                <div id="settings-wrapper">
                    <button id="toggleAutoRefresh" title="Pause/start the auto-refreshing of logs">Toggle Auto Refresh</button><br>
                    <button id="refresh" title="Manually refresh the logs">Refresh</button><br>
                    <button id="toggleGroupLogs" title="Group logs of the same user or not">Toggle Group Logs</button>
                </div>
            </div>
        </div>

        <h1>Logs</h1>
        <span id="filters-applied">No filters applied.</span>
        <br><br>
        <div id="logs"></div>

        <script>
            let auth_cookie = getCookie("AJAX_AUTH");

            function addEventListenerForOperators() {
                let operators = document.getElementsByClassName("operator");
                for (let i = 0; i < operators.length; i++) {
                    operators[i].addEventListener("click", changeOperator);
                }

                let filterInputs = document.getElementsByClassName("updateOnChange");
                for (let i = 0; i < filterInputs.length; i++) {
                    filterInputs[i].addEventListener("input", function(){autoRefreshLogs(true)});
                    filterInputs[i].addEventListener("change", function(){autoRefreshLogs(true)});
                }
            }

            addEventListenerForOperators();
            document.getElementById("addFilter").addEventListener("click", addFilter);
            document.getElementById("toggleAutoRefresh").addEventListener("click", toggleAutoRefresh);
            document.getElementById("toggleGroupLogs").addEventListener("click", toggleGroupLogs);
            document.getElementById("refresh").addEventListener("click", function(){ autoRefreshLogs(true) });
            
            operators = [
                "=",
                "\u2248",
                "\u2260",
                ">",
                "<",
                "\u2265",
                "\u2264"
            ];
            
            operators_ = [
                "=",
                "LIKE",
                "!=",
                ">",
                "<",
                ">=",
                "<="
            ];
    
            autoRefresh = true;
            groupLogs = true;
            
            autoRefreshLogs();
            autoRefreshInterval = setInterval(autoRefreshLogs, 3000);
            
            function toggleAutoRefresh() {
                if (autoRefresh) {
                    autoRefresh = false;
                    clearInterval(autoRefreshInterval);
                } else {
                    autoRefresh = true;
                    autoRefreshInterval = setInterval(autoRefreshLogs, 3000);
                }
            }

            function toggleGroupLogs() {
                if (groupLogs)
                    groupLogs = false;
                else
                    groupLogs = true;
            }

            function addFilter() {
                let filters = $("#filter-wrapper");
                let filterHtml = `
                    <div class="filter">
                        <select id="column" name="column" class="updateOnChange">
                            <option value='userID'>User ID</option>
                            <option value='PaymentID'>Payment ID</option>
                            <option value='username'>Username</option>
                            <option value='email'>E-mail</option>
                            <option value='datetime'>Date</option>
                        </select>

                        <input type="text" data-value="0" value="=" class="operator" name="operator" readonly />

                        <input type="text" name="filterValue" placeholder="Value" id="filterValue" class="updateOnChange" />
                    </div> 
                `;

                filters.append(filterHtml);
                addEventListenerForOperators();
            }

            
            function changeOperator(event) {
                let caller = $(event.target);


                let i = caller.attr("data-value");
                if (i == operators.length-1) i = -1;
                i++;
                caller.attr("data-value", i);
                caller.val(operators[i]);
            }

            function autoRefreshLogs(manual = false) {
                filterStr = "";
                let counter = 0;
                
                $("#filters-applied").html("No filters applied.");
                $(".filter").each(function( i ) {
                    let column = $(this).find("#column").val();
                    let operator = $(this).find(".operator").val();
                    let filterValue = $(this).find("#filterValue").val();
                    
                    if (column == "" ||
                        operator == "" ||
                        filterValue == "")
                        return;

                    //Column has string as datatype
                    if (column != "userID") {
                        filterValue = "{" + filterValue + "}";
                    }

                    //Replace visual operator with actual operator
                    for (let i = 0; i < operators.length; i++) {
                        if (operators[i] == operator) {
                            operator = operators_[i];
                            break;
                        }
                    }
                    
                    filterStr += column + " " + operator + " " + filterValue + ";";
                    counter++;
                    $("#filters-applied").html(counter + " filters applied.");
                });

                let xmlhttp = new XMLHttpRequest();

                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        if (this.responseText != "NNR" || manual) //No New Rows
                            $("#logs").html(this.responseText);
                    }
                };

                let groupParam = "&groupByUser=0";
                if (groupLogs) groupParam = "&groupByUser=1";
                xmlhttp.open("GET", "../php/adminPanelCode/LogHandler.php?filters=" + filterStr.substring(0, filterStr.length-1) + groupParam + "&auth=" + auth_cookie + "&rowAmount=" + ($("#logs span").length / 2), true);

                console.log(($("#logs span").length / 2));
                xmlhttp.send();

            }

            function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
        </script>
    </body>
</html>

