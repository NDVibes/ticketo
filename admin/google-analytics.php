<?php
    include_once('../config/config.php');
    include_once(PATH . '/php/classes/Auth.php');
    include_once(PATH . '/php/classes/Log.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 1;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'google-analytics.php';
    $auth->permission = 1;
    $_SESSION['auth'] = $auth;
    $auth->prompt();


?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
		

        <style>
            body {
                background-color: #1C1C1C;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            h1 {
                padding-bottom: 0;
                margin-bottom: 5px;
            }
            #container {
                min-height: 150px;
                max-height: 250px;
                background-color: #2E2E2E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            #search-container,
            #settings-container {
                width: 50%;
                float: left;
            }

            #search-container .title,
            #settings-container .title {
                font-size: 120%;
                font-weight: bold;
            }
            #main {
                max-width: 1050px;
                margin: auto;
                padding-bottom: 150px;
            }

            .section form span {
                display: block;
            }
            .section form input[type="text"] {
                width: 250px;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="main">

            <!-- Step 1: Create the containing elements. -->

            <section id="auth-button"></section>
            <section id="view-selector"></section>
            <section id="timeline"></section>

        </div>

        <!-- Step 2: Load the library. -->

        <script>
        (function(w,d,s,g,js,fjs){
          g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
          js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
          js.src='https://apis.google.com/js/platform.js';
          fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
        }(window,document,'script'));
        </script>

        <script>
        gapi.analytics.ready(function() {

          // Step 3: Authorize the user.

          var CLIENT_ID = 'Insert your client ID here';

          gapi.analytics.auth.authorize({
            container: 'auth-button',
            clientid: CLIENT_ID,
          });

          // Step 4: Create the view selector.

          var viewSelector = new gapi.analytics.ViewSelector({
            container: 'view-selector'
          });

          // Step 5: Create the timeline chart.

          var timeline = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
              'dimensions': 'ga:date',
              'metrics': 'ga:sessions',
              'start-date': '30daysAgo',
              'end-date': 'yesterday',
            },
            chart: {
              type: 'LINE',
              container: 'timeline'
            }
          });

          // Step 6: Hook up the components to work together.

          gapi.analytics.auth.on('success', function(response) {
            viewSelector.execute();
          });

          viewSelector.on('change', function(ids) {
            var newIds = {
              query: {
                ids: ids
              }
            }
            timeline.set(newIds).execute();
          });
        });
        </script>
    </body>
</html>

