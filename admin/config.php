<?php
    include_once('../config/config.php');
    include_once(PATH . '/php/classes/Auth.php');
    include_once(PATH . '/php/classes/Log.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 2;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'config.php';
    $auth->permission = 2;
    $_SESSION['auth'] = $auth;
    $auth->prompt();

    $nielson = new Nielson(PATH . "/config/systemconfig.nielson.php");
    $nielson->read();

    $debug = $nielson->getObject('setting', 'debug');
    $hostAndPaths = $nielson->getObject('setting', 'HostAndPaths');
    $paymentApi = $nielson->getObject('setting', 'PaymentAPI');
    $database = $nielson->getObject('setting', 'Database');
    $mail = $nielson->getObject('setting', 'Mail');

?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
		

        <style>
            body {
                background-color: #1C1C1C;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            h1 {
                padding-bottom: 0;
                margin-bottom: 5px;
            }
            #container {
                min-height: 150px;
                max-height: 250px;
                background-color: #2E2E2E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            #search-container,
            #settings-container {
                width: 50%;
                float: left;
            }

            #search-container .title,
            #settings-container .title {
                font-size: 120%;
                font-weight: bold;
            }
            #main {
                max-width: 1050px;
                margin: auto;
                padding-bottom: 150px;
            }

            .section form span {
                display: block;
            }
            .section form input[type="text"] {
                width: 250px;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="main">
            <div id="container">
                <div id="settings-container">
                    <span class="title">Config View Settings</span><br><br>
                </div>
            </div>

            <h1>Config Settings</h1>
            <br><br>
            <div id="config-container">
                <div class="section">
                    <form method="post" action="../php/adminPanelCode/configHandler.php">
                        <input type="hidden" value="" class="auth" name="auth" />
                        <h2>Debug</h2>
                        <span>Toggle Debug On/Off (Currently: <?php echo $debug->getProperty("value")->value; ?>)</span>
                        <input type="submit" name="btnDebug" value="Toggle " />
                    </form>
                </div>
                <div class="section">
                    <h2>Hosts & paths</h2>
                    <form method="post" action="../php/adminPanelCode/configHandler.php">
                        <input type="hidden" value="" class="auth" name="auth" />
                        <span>Host (e.g: https://website.com/ticketo)</span>
                        <input type="text" name="host" value="<?php echo $hostAndPaths->getProperty("Host")->value; ?>"/>
                        
                        <br><br>
                        <input type="submit" name="btnHostAndPaths" value="Update" />
                    </form>
                </div>
                <div class="section">
                    <h2>Payment API</h2>
                    <form method="post" action="../php/adminPanelCode/configHandler.php">
                        <input type="hidden" value="" class="auth" name="auth" />
                        <span>Mollie API-key (Live/Debug)</span>
                        <input type="text" name="mollie-key" value="<?php echo $paymentApi->getProperty("MollieKey")->value; ?>"/>
                        
                        <br><br>
                        <input type="submit" name="btnPaymentApi" value="Update" />
                    <form>
                </div>
                <div class="section">
                    <h2>Database</h2>
                    <form method="post" action="../php/adminPanelCode/configHandler.php">
                        <input type="hidden" value="" class="auth" name="auth" />
                        <span>Set Database Host (e.g: localhost)</span><br>
                        <input type="text" name="database-host" value="<?php echo $database->getProperty("Host")->value; ?>"/>
                        <br><br>
                        
                        <span>Set Database User (e.g: root)</span><br>
                        <input type="text" name="database-user" value="<?php echo $database->getProperty("User")->value; ?>"/>
                        <br><br>
                        
                        <span>Set Database Password</span><br>
                        <input type="password" name="database-pass" value="<?php echo $database->getProperty("Pass")->value; ?>" />
                        <br><br>
                        
                        <span>Set Database Name (e.g TicketSystem)</span><br>
                        <input type="text" name="database-name" value="<?php echo $database->getProperty("Database")->value; ?>" />
                        
                        <br><br>
                        <input type="submit" name="btnDatabase" value="Update" />
                    <form>
                </div>
                <div class="section">
                    <h2>Mail</h2>
                    <form method="post" action="../php/adminPanelCode/configHandler.php">
                        <input type="hidden" value="" class="auth" name="auth" />
                        <span>Set Mailhost (e.g: mail.host.com)</span><br>
                        <input type="text" name="mail-host" value="<?php echo $mail->getProperty("Host")->value; ?>"/>
                        <br><br>
                        
                        <span>Set Mail Port (e.g: 465)</span><br>
                        <input type="number" name="mail-port" value="<?php echo $mail->getProperty("Port")->value; ?>"/>
                        <br><br>
                        
                        <span>Set Mail User (e.g: info@website.com)</span><br>
                        <input type="email" name="mail-user" value="<?php echo $mail->getProperty("User")->value; ?>"/>
                        <br><br>
                        
                        <span>Set Mail From (e.g: info@website.com) - often the same as Mail User</span><br>
                        <input type="text" name="mail-from" value="<?php echo $mail->getProperty("From")->value; ?>"/>
                        <br><br>
                        
                        <span>Set Mail Password</span><br>
                        <input type="password" name="mail-pass" value="<?php echo $mail->getProperty("Pass")->value; ?>" />
                        
                        <br><br>
                        <input type="submit" name="btnMail" value="Update" />
                    <form>
                </div>
            </div>
        </div>

        <script>
            window.onload = function() {
                $(".auth").each(function() {
                    $(this).val(getCookie("AJAX_AUTH"));
                });
            }


            function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
        </script>
    </body>
</html>

