<?php
    include_once('../config/config.php');
    include_once(PATH . 'php/classes/Auth.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 2;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'manage-admins.php';
    $auth->permission = 2;
    $_SESSION['auth'] = $auth;
    $auth->prompt();
?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
		

        <style>
            body {
                background-color: #000000;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            h1 {
                padding-bottom: 0;
                margin-bottom: 5px;
            }
            #container {
                width: 1150px;
                margin: auto;
            }
            #header-container {
                min-height: 150px;
                max-height: 250px;
                background-color: #6E6E6E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            table, tr, td, th {
                border-collapse: collapse;
                border: solid 1px #002B02;
                padding: 10px;
            }
            table {
                margin: auto;
                width: 100%;
            }
            #btnAddRow {
                background-color: transparent;
                border: 0;
                color: #6E6E6E;
                padding: 0;
                margin: 0;
                margin-top: 5px;
                cursor: pointer;
                font-weight: bold;
                font-size: 0.90em;
            }

            td input {
                background-color: #111111;
                border: 0;
                color: #FFFFFF;
                font-weight: bold;
                padding: 5px;
                width: 100%;
                box-sizing: border-box;
                font-size: 1em;
                caret-color: #00FF00;
            }
            #btnAddAdmins {
                margin-top: 10px;
                float: right;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="container">
            <div id="header-container">


            </div>
            <h1>Manage Admins</h1>
            <div id="option-container">
                <div id="view">
                    <h2>View admins</h2>
                    <table>
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>E-mail</th>
                            <th>Permission</th>
                        </tr>

                        <?php
                            $conn = new mysqli(DBHOST, USER, PASS, DTBS);
                            $stmt = $conn->prepare("SELECT ID, admin, email, permission FROM admins");
                            $stmt->execute();
                            $stmt->bind_result($ID, $username, $email, $permission);

                            while ($stmt->fetch()) {
                                $strPermission = "Allowed to view";
                                if ($permission == 2)
                                    $strPermission = "Allowed to view and configure";

                                echo "
                                    <tr>
                                        <td>".$ID."</td>
                                        <td>".htmlspecialchars($username)."</td>
                                        <td>".htmlspecialchars($email)."</td>
                                        <td>".$strPermission."</td>
                                    </tr>
                                ";
                            }

                            $stmt->close();
                        ?>
                    </table>
                </div>
                <div id="add">
                    <h2>Add admin manually</h2>
                    <form method="post" name="" action="../php/adminPanelCode/manageAdminHandler.php" id="tableForm">
                        <input type="hidden" value="" class="auth" name="auth" />
                    <table id="add-admin">
                        <thead>
                            <tr>
                                <td>Username</td>
                                <td>Password</td>
                                <td>E-mail</td>
                                <td>Permission</td>
                            </tr>
                        </thead>
                            <tbody id="tableBody">
                                    <tr>
                                        <td>
                                            <input type="text" name="username[]" class="input" placeholder="Loginname" required />
                                        </td>
                                        <td>
                                            <input type="password" name="password[]" class="input" placeholder="Password" required />
                                        </td>
                                        <td>
                                            <input type="email" name="email[]" class="input" placeholder="example@email.com" required />
                                        </td>
                                        <td>
                                            <select name="permission[]" required>
                                                <option value="1">Allow to view only</option>
                                                <option value="2">Allow to view and configure</option>
                                            </select>
                                        </td>
                                    </tr>
                            </tbody>
                            
                    </table>
                    <input type="submit" value="Add all admins in table" name="btnAddAdmins" id="btnAddAdmins" />
                     </form>
                    <button id="btnAddRow">+ Add row</button>
                </div>
                <div id="delete">
                    <h2>Delete admin</h2>
                    <form method="POST" action="../php/adminPanelCode/manageAdminHandler.php">
                        <input type="hidden" value="" class="auth" name="auth" />
                        <select name="toDelete">
                            <?php
                                $stmt = $conn->prepare("SELECT ID, admin FROM admins");
                                $stmt->execute();
                                $stmt->bind_result($ID, $username);

                                while ($stmt->fetch()) {
                                    echo "<option value='".htmlspecialchars($ID)."'>" . $ID . " - " . htmlspecialchars($username) . "</option>";
                                }

                                $stmt->close();
                            ?>
                        </select>
                        <input type="submit" value="Delete admin" name="btnDeleteAdmin" />
                    </form>
                </div>
            </div>
        </div>
        <script>
            window.onload = function() {
                $(".auth").each(function() {
                    $(this).val(getCookie("AJAX_AUTH"));
                });
            }
            
            document.getElementById("btnAddRow").addEventListener("click", addRow);

            function addRow() {
                html = `
                    <tr>
                        <td>
                            <input type="text" name="username[]" class="input" placeholder="Loginname" required />
                        </td>
                        <td>
                            <input type="password" name="password[]" class="input" placeholder="Password" required />
                        </td>
                        <td>
                            <input type="email" name="email[]" class="input" placeholder="example@email.com" required />
                        </td>
                        <td>
                            <select name="permission[]" required>
                                <option value="1">Allow to view only</option>
                                <option value="2">Allow to view and configure</option>
                            </select>
                        </td>
                    </tr>
                `;

                $("#tableBody").append(html);
            }
            
            function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
        </script>
    </body>
</html>

