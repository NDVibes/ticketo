<?php
    include_once('../config/config.php');
    include_once(PATH . '/php/classes/Auth.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 1;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'index.php';
    $auth->neededPermission = $NEEDED_PERMISSION;
    $_SESSION['auth'] = $auth;
    $auth->prompt();
?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
        <style>
            body {
                background-color: #1C1C1C;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            #container {
                width: 1000px;
                margin: auto;
                padding-bottom: 150px;
            }
            #container h1 {
                text-align: center;
            }
        </style>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
        <script src="../js/admin.js"></script>
      
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="container">
            <h1>ADMIN PANEL</h1>

            <?php
                readfile(getcwd() . "/controls.html");
            ?>
        </div>
    </body>
</html>
