<?php
    include_once('../php/Nielson/Nielson.php');
 
    session_start();
    ini_set('display_errors', 1);

    if (file_exists( "../config/init_finished.txt" )) {
        header('Location: index.php');
        die();
    }


    $AUTH_TOKEN = "abc123";

    //Function to check if authentication_token is succesfully given 
    function is_authenticated() {
        if (!isset($_SESSION['init_auth'])) {
            header('Location: ../index.php');
            return false;
        }

        return true;
    }
    
    //Function to update database-configuration
    function databaseConfig($dbhost, $dbuser, $dbpass, $db) {
        $nielson = new Nielson("../config/systemconfig.nielson.php");   
        $nielson->hide_content = true;
        $nielson->read();
        
        $object = $nielson->getObject('setting', 'Database');

        $object->getProperty('Host')->value = $dbhost;
        $object->getProperty('User')->value = $dbuser;
        $object->getProperty('Pass')->value = $dbpass;
        $object->getProperty('Database')->value = $db;

        $nielson->update();
    }
    
    //Function to add new admin-account to the database
    function createAdmin($user, $email, $pass) {
        //Get database credentials
        $nielson = new Nielson("../config/systemconfig.nielson.php");
        $nielson->read();

        $object = $nielson->getObject('setting', 'Database');
        
        $dbhost = $object->getProperty('Host')->value;
        $dbuser = $object->getProperty('User')->value;
        $dbpass = $object->getProperty('Pass')->value;
        $dbname = $object->getProperty('Database')->value;
        
        $conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
        if ($conn->error) {
            echo $conn->error;
            die();
        }

        $stmt = $conn->prepare("INSERT INTO admins (admin, email, password, permission) VALUES (?, ?, ?, ?)");
        
        $stmt->bind_param("sssi", $admin_, $email_, $pass_, $permission_);
        $admin_ = $user;
        $email_ = $email;
        $pass_ = password_hash($pass, PASSWORD_DEFAULT);
        $permission_ = 2;

        $stmt->execute();
        if ($stmt->error) {
            echo $stmt->error;
            die();
        }
    }

    //Function to update host and path config
    function hostAndPath($host, $path) {
        $nielson = new Nielson("../config/systemconfig.nielson.php");
        $nielson->hide_content = true;
        $nielson->read();

        //Update host
        $object = $nielson->getObject('setting', 'HostAndPaths');

        $object->getProperty('Host')->value = $host;
        $nielson->update();

        //Update path to TicketoFolder
        $content = file_get_contents("../config/config.php");
        $content = str_replace("%path%", "'" . $path . "'", $content);
        file_put_contents("../config/config.php", $content);
    }

    //Step one
    if (isset($_POST['btnOne'])) {
        if ($_POST['auth_token'] == $AUTH_TOKEN) {
            $_SESSION['init_auth'] = true;
            echo "<script>window.onload = function() {changeStep('two');}</script>";
            unset($_POST['btnOne']);
        } else {
            header('Location: ../index.php');
        }
    }

    //Step two
    else if (isset($_POST['btnTwo'])) {
        if (is_authenticated()) {
            databaseConfig($_POST['db-host'], $_POST['db-user'], $_POST['db-pass'], $_POST['db-name']);
            echo "<script>window.onload = function() {changeStep('three');}</script>";
        }
    }
    
    //Step three
    else if (isset($_POST['btnThree'])) {
        if (is_authenticated()) {
            echo "<script>window.onload = function() {changeStep('four');}</script>";
        }
    }
    
    //Step four
    else if (isset($_POST['btnFour'])) {
        if (is_authenticated()) {
            createAdmin($_POST['admin-name'], $_POST['admin-email'], $_POST['admin-pass']);
            echo "<script>window.onload = function() {changeStep('five');}</script>";
        }
    }

    //Step five
    else if (isset($_POST['btnFive'])) {
        if (is_authenticated()) {
            hostAndPath($_POST['host'], $_POST['path']);
            echo "<script>window.onload = function() {changeStep('six');}</script>";

            $file = fopen("../config/init_finished.txt", 'w');
            fwrite($file, date('Y-m-d H:i:s'));
            fclose($file);
            
        }
    }

    else {
        echo "<script>window.onload = function() {changeStep('one');}</script>";
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			  crossorigin="anonymous"></script>

        <script src="../js/general.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/general.css" >
        <style>
            body {
                background-color: #151515;
                color: #FFFFFF;
                font-family: sans-serif;
                text-align: center;
            }
            #step-container {
                width: 90%;
                max-width: 850px;
                height: auto;
                min-height: 425px;
                margin: auto;
                margin-top: 50px;
                background-color: #1C1C1C;
                border-radius: 10px;
                padding: 20px;
                box-sizing: border-box;
                position: relative;
            }
            h2 {
                font-family: sans-serif;
                letter-spacing: 0;
            }
            #step-container .progress { bottom: 25px; }

            form {
                margin-top: 35px;
            }

            input[type='text'],
            input[type='email'], 
            input[type='password'] {
                background-color: #151515;
                border: 0;
                border-radius: 10px;
                padding: 8px;
                font-size: 1.2em;
                width: 300px;
                color: #FFFFFF;
                caret-color: #FF6600;
                font-weight: bold;
                text-align: center;
                font-style: normal;
            }
            input[type='submit'],
            .step#six a {
                background-color: transparent;
                border: 0;
                color: #FF6600;
                font-weight: bold;
                font-size: 0.9em;
                margin-top: 15px;
                cursor: pointer;
                text-decoration: none;
            }
            .step#six a {
                margin-top: 50px !important;
            }
            .step {
                display: none;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h1>Ticketo Initialistation</h1>
            </div>
            <div id="step-container">
                <div class="step" id="one">
                    <h2>Please give in the authentication token</h2>

                    <form method="post" action="">
                        <input type="text" name="auth_token" />
                        <br>
                        <input type="submit" value="Next" name="btnOne" />
                    </form>

                    <script>
                        showProgress(0, 5);
                    </script>
                </div>
                <div class="step" id="two">
                    <h2>First things first... Let's configure the Database</h2>

                    <form method="post" action="">
                        <input type="text" name="db-name" value="TicketSystem" placeholder="Database Name" />
                        <br>
                        <input type="text" name="db-host" placeholder="Database Host" />
                        <br>
                        <input type="text" name="db-user" placeholder="Database User" />
                        <br>
                        <input type="password" name="db-pass" placeholder="Database Password" />
                        <br>
                        <input type="submit" value="Next" name="btnTwo" />
                    </form>

                    <script>
                        showProgress(1, 5);
                    </script>
                </div>
                <div class="step" id="three">
                    <h2>Hello there, enter your name</h2>

                    <form method="post" action="">
                        <input type="text" name="name" />
                        <br>
                        <input type="submit" value="Next" name="btnThree" />
                    </form>

                    <script>
                        showProgress(2, 5);
                    </script>
                </div>
                <div class="step" id="four">
                    <h2>Welcome to Ticketo <?php echo htmlspecialchars($_POST['name']); ?>, let's make your admin account</h2>

                    <form method="post" action="">
                        <input type="text" name="admin-name" value="<?php echo htmlspecialchars( explode(' ', $_POST['name'] )[0] ); ?>" placeholder="Loginname"/>
                        <br>
                        <input type="email" name="admin-email" placeholder="admin@mail.com" />
                        <br>
                        <input type="password" name="admin-pass" placeholder="Password" />
                        <br>
                        <input type="submit" value="Next" name="btnFour" />
                    </form>

                    <script>
                        showProgress(3, 5);
                    </script>
                </div>
                <div class="step" id="five">
                    <h2>Set the host and path so we know where we are</h2>

                    <form method="post" action="">
                        
                        <input type="text" name="host" placeholder="Host*" />
                        <br>
                        <input type="text" name="path" placeholder="TicketoFolder*" />
                        <br>

                        <div class="help">
                            *Host: The host and directory to the ticketo folder. E.g: https://your-website.com/buy/ticketo<br>
                            *Path: The path to the ticketo directory. If https://your-website.com/buy/ticketo is your host, /buy/ticketo/ is your TicketoFolder.
                        </div>

                        <input type="submit" value="Next" name="btnFive" />
                    </form>

                    <script>
                        showProgress(4, 5);
                    </script>
                </div>
                <div class="step" id="six">
                    <h2>Congratulations! Ticketo is intialised, we you'll be redirected to the admin-panel to configure the remaining options.</h2>

                        
                    <a href="config.php">Finish</a>
                    <script>
                        showProgress(5, 5);
                    </script>
                </div>
            </div>
        </div>

        <script>
            function changeStep(step) {
                //Hide all
                $(".step").fadeOut();
                $(".step#" + step).delay(500).fadeIn();
            }
        </script>
    </body>
</html>
