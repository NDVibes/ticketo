<?php
    include_once('../config/config.php');
    include_once(PATH . 'php/classes/Auth.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 2;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'manage-coupons.php';
    $auth->permission = 2;
    $_SESSION['auth'] = $auth;
    $auth->prompt();
?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
		

        <style>
            body {
                background-color: #000000;
                color: #FFFFFF;
                font-family: sans-serif;
            }
            h1 {
                padding-bottom: 0;
                margin-bottom: 5px;
            }
            #container {
                width: 1150px;
                margin: auto;
            }
            #header-container {
                min-height: 150px;
                max-height: 250px;
                background-color: #6E6E6E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            table, tr, td, th {
                border-collapse: collapse;
                border: solid 1px #002B02;
                padding: 10px;
            }
            table {
                margin: auto;
                width: 100%;
            }
            #btnAddRow {
                background-color: transparent;
                border: 0;
                color: #6E6E6E;
                padding: 0;
                margin: 0;
                margin-top: 5px;
                cursor: pointer;
                font-weight: bold;
                font-size: 0.90em;
            }

            td input {
                background-color: #111111;
                border: 0;
                color: #FFFFFF;
                font-weight: bold;
                padding: 5px;
                width: 100%;
                box-sizing: border-box;
                font-size: 1em;
                caret-color: #00FF00;
            }
            #btnAddCoupons {
                margin-top: 10px;
                float: right;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body>
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="container">
            <div id="header-container">


            </div>
            <h1>Manage Coupons</h1>
            <div id="option-container">
                <div id="view">
                    <h2>View coupons</h2>
                    <table>
                        <tr>
                            <th>ID</th>
                            <th>Coupon</th>
                            <th>Discount</th>
                            <th>Available Uses</th>
                        </tr>

                        <?php
                            $conn = new mysqli(DBHOST, USER, PASS, DTBS);
                            $stmt = $conn->prepare("SELECT ID, coupon, discount, available_uses FROM coupons");
                            $stmt->execute();
                            $stmt->bind_result($ID, $coupon, $discount, $available_uses);

                            while ($stmt->fetch()) {
                                echo "
                                    <tr>
                                        <td>".$ID."</td>
                                        <td>".htmlspecialchars($coupon)."</td>
                                        <td>-".htmlspecialchars($discount)."%</td>
                                        <td>".htmlspecialchars($available_uses)."</td>
                                    </tr>
                                ";
                            }

                            $stmt->close();
                        ?>
                    </table>
                </div>
                <div id="add">
                    <h2>Add coupon manually</h2>
                    <form method="post" name="" action="../php/adminPanelCode/manageCouponHandler.php" id="tableForm">
                        <input type="hidden" value="" class="auth" name="auth" />
                    <table id="add-coupon">
                        <thead>
                            <tr>
                                <td>Coupon</td>
                                <td>Discount</td>
                                <td>Available Uses</td>
                            </tr>
                        </thead>
                            <tbody id="tableBody">
                                    <tr>
                                        <td>
                                            <input type="text" name="coupon[]" class="input" placeholder="Coupon code" required />
                                        </td>
                                        <td>
                                            <input type="number" min="0" max="100" name="discount[]" class="input" placeholder="Discount in %" required />
                                        </td>
                                        <td>
                                            <input type="text" name="available_uses[]" class="input" placeholder="Amount of uses" value="INFINITE" required />
                                        </td>
                                    </tr>
                            </tbody>
                            
                    </table>
                    <input type="submit" value="Add all coupons in table" name="btnAddCoupons" id="btnAddCoupons" />
                     </form>
                    <button id="btnAddRow">+ Add row</button>
                </div>
                <div id="delete">
                    <h2>Delete coupon</h2>
                    <form method="POST" action="../php/adminPanelCode/manageCouponHandler.php">
                        <input type="hidden" value="" class="auth" name="auth" />
                        <select name="toDelete">
                            <?php
                                $stmt = $conn->prepare("SELECT ID, coupon FROM coupons");
                                $stmt->execute();
                                $stmt->bind_result($ID, $coupon);

                                while ($stmt->fetch()) {
                                    echo "<option value='".htmlspecialchars($ID)."'>" . $ID . " - " . htmlspecialchars($coupon) . "</option>";
                                }

                                $stmt->close();
                            ?>
                        </select>
                        <input type="submit" value="Delete coupon" name="btnDeleteCoupon" />
                    </form>
                </div>
            </div>
        </div>
        <script>
            window.onload = function() {
                $(".auth").each(function() {
                    $(this).val(getCookie("AJAX_AUTH"));
                });
            }
            
            document.getElementById("btnAddRow").addEventListener("click", addRow);

            function addRow() {
                html = `
                    <tr>
                        <td>
                            <input type="text" name="coupon[]" class="input" placeholder="Coupon code" required />
                        </td>
                        <td>
                            <input type="number" min="0" max="100" name="discount[]" class="input" placeholder="Discount in %" required />
                        </td>
                        <td>
                            <input type="text" name="available_uses[]" class="input" placeholder="Amount of uses" value="INFINITE" required />
                        </td>
                    </tr>
                `;

                $("#tableBody").append(html);
            }
            
            function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
        </script>
    </body>
</html>

