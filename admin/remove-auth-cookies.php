<?php
    include_once('../config/config.php');
    include_once('../php/classes/Auth.php');
    include_once('../php/classes/Log.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 2;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'index.php';
    $auth->permission = 2;
    $_SESSION['auth'] = $auth;
    $auth->prompt();

    $file = fopen("AUTH_COOKIES/authcookie.txt.php", 'w');
    fwrite($file, '<?php $HIDE_CONTENT = "' . PHP_EOL . ":");
    fclose($file);

    header("Location: logout.php");
