<?php
    include_once('../config/config.php');
    include_once('../php/classes/Auth.php');
    session_start();

    //Authenticate
    $NEEDED_PERMISSION = 1;
    if ($_SESSION['auth']->permission < $NEEDED_PERMISSION) unset($_SESSION['auth']);
    
    if (!isset($_SESSION['auth'])) {
        $auth = new Auth();
    } else $auth = $_SESSION['auth'];
    $auth->authUrl = 'auth.php';
    $auth->ret = 'mails.php';
    $auth->permission = 1;
    $_SESSION['auth'] = $auth;
    $auth->prompt();
?>

<!DOCTYPE html>
<html>
    <head>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            body {
                background-color: #111111;
                color: #FFFFFF;
                font-family: Arial;
            }
            #container {
                width: 1150px;
                margin: auto;
            }
            table, tr, td, th {
                border-collapse: collapse;
                border: solid 1px #0B3B0B;
                padding: 10px;
            }
            table th { background-color: #1C1C1C; }
            table {
                margin: auto;
                width: 100%;
            }
            #meta {
              text-align: right;
              margin-bottom: 5px;
            }
            #filters-applied {
                text-align: left;
                float: left;
            }

            #header {
                min-height: 150px;
                max-height: 250px;
                background-color: #2E2E2E;
                border: solid 2px #2E2E2E;
                margin-bottom: 30px;
                overflow-y: scroll;
                padding: 10px;
            }
            
            #search-container,
            #settings-container {
                width: 50%;
                float: left;
            }

            #search-container .title,
            #settings-container .title {
                font-size: 120%;
                font-weight: bold;
            }
            .filter {
                margin-top: 5px;
            }
            .filter select {
                background-color: #1C1C1C;
                color: #FF6600;
                padding: 6px;
                border: 0;
                width: 125px;
            }
            .filter select option {
                background-color: #1C1C1C !important;
                color: #FF6600;
                padding: 6px;
                border: 0;
                width: 125px;
            }
            .filter .operator {
                padding: 6px;
                width: 30px;
                margin: 0;
                background-color: #1C1C1C;
                color: #FF6600;
                border: 0;
                text-align: center;
                cursor: pointer;
            }
            .filter #filterValue {
                padding: 6px;
                border: 0;
                background-color: #1C1C1C;
                color: #FF6600;
                font-weight: bold;
            }

            #addFilter {
                background-color: transparent;
                border: 0;
                color: #848484;
                padding: 0;
                margin: 0;
                cursor: pointer;
                font-weight: bold;
            }
            #settings-wrapper * {
                margin-bottom: 10px;
            }
        </style>
        <script src="../js/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="../style/admin.css">
    </head>
    <body> 
        <?php
            include_once('../partials/adminmenu.php');
            menu();
        ?>
        <div id="container"> 
            <div id="header">
                <div id="search-container">
                    <span class="title">Search mail</span><br>
                    <span>Display rows where...</span><br>
                    <div id="filter-wrapper">
                        <div class="filter">
                            <select id="column" name="column" class="updateOnChange">
                                <option value='ID'>ID</option>
                                <option value='firstname'>Firstname</option>
                                <option value='lastname'>Lastname</option>
                                <option value='email'>E-mail</option>
                                <option value='date'>Date</option>
                                <option value='paid'>Paid</option>
                            </select>

                            <input type="text" data-value="0" value="=" class="operator updateOnChange" name="operator" readonly />

                            <input type="text" name="filterValue" placeholder="Value" id="filterValue" class="updateOnChange" />
                        </div>
                    </div>
                    <br><button id="addFilter">+ Add filter</button>
                </div>
                <div id="settings-container">
                    <span class="title">Database View Settings</span><br><br>
                    <div id="settings-wrapper">
                        <button id="toggleAutoRefresh" title="Pause/start the auto-refreshing of logs">Toggle Auto Refresh</button><br>
                        <button id="refresh" title="Manually refresh the database">Refresh</button><br>
                    </div>
                </div>
            </div>

            <span id="filters-applied">No filters applied.</span>
            <div id="meta">
                <span class="label">Total: </span><span id="total">0</span>
                <span class="label">Paid: </span><span id="paid">0</span>
            </div>
            
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>E-mail</th>
                        <th>Paid</th>
                        <th>Date</th>
                        <th>Send Ticket By Mail</th>
                    </tr>
                </thead>
                <tbody id="database-table"></tbody>
            </table>
        </div>

        <script>
            function addEventListenerForOperators() {
                let operators = document.getElementsByClassName("operator");
                for (let i = 0; i < operators.length; i++) {
                    operators[i].addEventListener("click", changeOperator);
                }

                let filterInputs = document.getElementsByClassName("updateOnChange");
                for (let i = 0; i < filterInputs.length; i++) {
                    filterInputs[i].addEventListener("input", function(){autoRefreshDatabase(true)});
                    filterInputs[i].addEventListener("change", function(){autoRefreshDatabase(true)});
                }
            }

            addEventListenerForOperators();
            document.getElementById("addFilter").addEventListener("click", addFilter);
            document.getElementById("toggleAutoRefresh").addEventListener("click", toggleAutoRefresh);
            document.getElementById("refresh").addEventListener("click", function(){autoRefreshDatabase(true)});
            
            operators = [
                "=",
                "\u2248",
                "\u2260",
                ">",
                "<",
                "\u2265",
                "\u2264"
            ];
            
            operators_ = [
                "=",
                "LIKE",
                "!=",
                ">",
                "<",
                ">=",
                "<="
            ];

            autoRefresh = true;
            
            autoRefreshDatabase();
            autoRefreshInterval = setInterval(autoRefreshDatabase, 5000);

            function toggleAutoRefresh() {
                if (autoRefresh) {
                    autoRefreshInterval = setInterval(autoRefreshDatabase, 5000);
                    autoRefresh = false;
                } else {
                    clearInterval(autoRefreshInterval);
                    autoRefresh = true;
                }
            }
            
            function addFilter() {
                let filters = $("#filter-wrapper");
                let filterHtml = `
                    <div class="filter">
                        <select id="column" name="column" id="updateOnChange">
                            <option value='ID'>ID</option>
                            <option value='firstname'>Firstname</option>
                            <option value='lastname'>Lastname</option>
                            <option value='email'>E-mail</option>
                            <option value='date'>Date</option>
                            <option value='paid'>Paid</option>
                        </select>

                        <input type="text" data-value="0" value="=" class="operator updateOnChange" name="operator" readonly />

                        <input type="text" name="filterValue" placeholder="Value" id="filterValue" class="updateOnChange" />
                    </div>
                `;

                filters.append(filterHtml);
                addEventListenerForOperators();
            }

            function changeOperator(event) {
                let caller = $(event.target);

                let i = caller.attr("data-value");
                if (i == operators.length-1) i = -1;
                i++;
                caller.attr("data-value", i);
                caller.val(operators[i]);
            }

            function autoRefreshDatabase(manual = false) { 
                filterStr = "";
                let counter = 0;
                
                $("#filters-applied").html("No filters applied.");
                $(".filter").each(function( i ) {
                    let column = $(this).find("#column").val();
                    let operator = $(this).find(".operator").val();
                    let filterValue = $(this).find("#filterValue").val();
                    
                    if (column == "" ||
                        operator == "" ||
                        filterValue == "")
                        return;

                    //Column has string as datatype
                    if (column != "userID" &&
                        column != "accepted_agreements" &&
                        column != "paid" &&
                        column != "scanned") {
                        filterValue = "{" + filterValue + "}";
                    }

                    //Convert yes/no to 1/0
                    if (column == "accepted_agreements" ||
                        column == "paid" ||
                        column == "scanned") {
                            if (filterValue.toLowerCase() == "no")
                                filterValue = "0";
                            else if (filterValue.toLowerCase() == "yes")
                                filterValue = "1";
                            else
                                filterValue = "0";
                        }

                    //Replace visual operator with actual operator
                    for (let i = 0; i < operators.length; i++) {
                        if (operators[i] == operator) {
                            operator = operators_[i];
                            break;
                        }
                    }

                    filterStr += column + " " + operator + " " + filterValue + ";";
                    counter++;
                    $("#filters-applied").html(counter + " filters applied.");
                });

                let xmlhttp = new XMLHttpRequest();

                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        if (this.responseText != "NNR" || manual) //No New Rows
                            $("#database-table").html(this.responseText);
                    }
                };

                xmlhttp.open("GET", "../php/adminPanelCode/MailHandler.php?filters=" + filterStr.substring(0, filterStr.length-1) + "&rowAmount=" + $("#database-table tr").length + "&auth=" + getCookie("AJAX_AUTH"), true);
                xmlhttp.send();
            } 
            
            function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
        </script>
    </body>
</html>
