<?php
    //Include
    include_once('config/config.php');
    include_once('classes/utils.php');
    include_once('classes/User.php');
    include_once('classes/Log.php');
    include_once('Nielson/Nielson.php');
	ini_set('display_errors', DEBUG);
    
    //Starting session
    session_start();

    $nielson = new Nielson(PATH . "/config/systemconfig.nielson.php");
    $nielson->read();
    $PRICE = $nielson->getObject('setting', 'Price')->getProperty("value")->value;

    //Terminate sales
    $date = date("Y-m-d H:i:s");
    $close = SHUTDOWN_TIME;
    if ($date >= date($close) && !isset($_SESSION['auth'])) {
      header("Location: shutdown.php");
    }

    unset($_SESSION['users']);

    if (isset($_POST['btnNext'])) {
        if (isset($_SESSION['users']) || isset($_SESSION['order'])) {
            header('Location: betaling.php');
        }

        $_SESSION['users'] = array();
        $_SESSION['temp_users'] = array();

        $i = 0;
        foreach ($_POST['email'] as $key => $n) {
            //Make user object
            $user = new User();

            //Input validation
            if ($_POST['firstname'][$key] == "") {
                error("Geen voornaam ingegeven.");
            } else {
                $user->firstname = $_POST['firstname'][$key];
            }
            if ($_POST['lastname'][$key] == "") {
                error("Geen achternaam ingegeven.");
            } else {
                $user->lastname = $_POST['lastname'][$key];
            }
            if (!filter_var($n, FILTER_VALIDATE_EMAIL)) {
                error("Geen geldig email-adres ingegeven.");
            } else {
                $user->email = $n;
            }
            if ($_POST['birthdate'][$key] == "" || validateDate($_POST['birthdate'][$key])) {
                error("Geen geldige geboortedatum ingegeven.");
            } else {
                $user->birthdate = $_POST['birthdate'][$key];
            }
            if ($_POST['accepted1'][0] == false || $_POST['accepted2'][0] == false || $_POST['accepted3'][0] == false) {
                error("Je moet de voorwaarden accepteren.");
            } else {
                if ($i > 0)
                    $user->accepted = false;
                else
                    $user->accepted = ($_POST['accepted1'][$key] && $_POST['accepted2'][$key] && $_POST['accepted3'][$key]);
            }
            $user->coupon = $_POST['coupon'];

            $_SESSION['temp_users'][] = $user;

            //Go to next stage in ordering-process
            //if user is valid
            if ($user->isValid()) {
                //Add user to session
                $_SESSION['users'][] = $user;
                
                //Starting logging system
                $_SESSION['log'] = new Log(0, $user->name(), $user->email); 
                $log = $_SESSION['log'];
                $log->write("Got validated, redirecting to check.php ...");
                $log->write("Used coupon code " . $user->coupon);

                header('Location: check.php');
            }

            $i++;
        }
    }
