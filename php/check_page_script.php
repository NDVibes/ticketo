<?php  
    //Include
    include_once('config/config.php');
    include_once('classes/utils.php');
    include_once('classes/User.php');
    include_once('classes/Log.php');
    
    //Starting session
    session_start();

    ini_set('display_errors', DEBUG);

    //Logging purchase-process and errors
    $log = $_SESSION['log'];
    
    //If session variable is not set,
    //user is on invalid page
    if (!isset($_SESSION['users'])) {
        $log->write("No user-session found on check.php, redirecting to error-page");
        
        header('Location: error.php');
        die();
    }
    
    //Retrieve user from session variable
    $users = $_SESSION['users'];
    
    //Start logging
    $log->username = $users[0]->name();
    $log->write("Device: " . htmlspecialchars($_SERVER['HTTP_USER_AGENT']));
    $log->write("Accessed the check-page");
