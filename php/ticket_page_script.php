<?php
    //Include
    include_once('config/config.php');
    include_once('classes/utils.php');
    include_once('classes/User.php');
    include_once("classes/Order.php");
    include_once("classes/Database.php");
    include_once("classes/Log.php");
    include_once("php/phpqrcode/qrlib.php");
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    require 'php/phpmailer/Exception.php';
    require 'php/phpmailer/PHPMailer.php';
    require 'php/phpmailer/SMTP.php';
    
    //Starting session
    session_start();

    //Debug mode
    ini_set('display_errors', DEBUG);
    
    //Check if user is allowed on ticket.php
    if (!isset($_SESSION['order']) && !isset($_GET['hash'])) {
        header('Location: error.php');
    }

    //Initiate database
    $db = new Database();

    //Initiate Order
    $order = NULL;
    if (!isset($_GET['hash']) && isset($_SESSION['order'])) {
        $order = $_SESSION['order'];
    }

    //Get ID and hash from database with paymentID
    $ID = 0;
    $paymentID = "";
    $hash = "xxx";
    if (!isset($_GET['hash'])) {
        $ID = $db->select($order->paymentID, 'ID');
        $hash = $db->select($order->paymentID, 'hash');
    } else {
        $stmt = $db->conn->prepare("SELECT ID, PaymentID, accepted_agreements FROM orders WHERE hash=?");
        $stmt->bind_param("s", $hash_);
        $hash_ = $_GET['hash'];
        $stmt->bind_result($dbID, $dbPaymentID, $accepted_agreements);
        $stmt->execute();
        $stmt->fetch();
        
        $ID = $dbID;
        $hash = $_GET['hash'];
        $paymentID = $dbPaymentID;

        if ($accepted_agreements == 0) {
            header('Location: accept_agreements.php?hash=' . $hash);
            die();
        }

        $stmt->close();
    }

    //Get order
    
    if (isset($_GET['hash'])) {
        $order = $db->get($ID);
    } 

    $log = new Log(0, "", "");
    if (isset($_SESSION['log'])) {
        $log = $_SESSION['log'];
    } else {
        $log = new Log($ID, $order->users[0]->name(), $order->users[0]->email);
    }
    
    //$log->paymentID = $order->paymentID;
    $log->write("Is succesfully redirected to the ticket.php page", 'success');
    
    //User must have paid to continue
    if (!$order->isPaid()) {
        $log->write("Order is not paid on ticket.php page, redirecting to error-page", 'failure');
       // header('Location: error.php');
    } else {
        //Data for QRCode
        $str = $ID . "," . $hash;
        $path = "qrcodes/" . $ID . "_" . $order->paymentID . ".png";
    
        //Generate QRCode(ID,hash)
        QRCode::png($str, $path, QR_ECLEVEL_L, 2);
        
        //Generate pseudo-code
        $pseudocode = pseudoCode($order->users[0], $order->ids[0]);
        
        $log->write("QRCode succesfully generated");
        $log->write("Accessed the mail-send code...");
        
        //Send mail
        /*
        $url = HOST . "/ticket.php?hash=" . $hash . "&view=true";
        $message = file_get_contents("partials/mail.html");
        $message = str_replace("%firstname%", $order->users[0]->firstname, $message);
        $message = str_replace("%url%", $url, $message);
        
        $mail = new PHPMailer();
        $mail->SMTPDebug = 1;
        $mail->isSMTP();
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;
        $mail->setFrom(MAIL_FROM, 'Bal Tropical Tickets');
        $mail->addAddress($order->users[0]->email, $order->users[0]->firstname . " " . $order->users[0]->lastname);
        $mail->Subject = 'Uw Bal Tropical ticket';
        $mail->AddEmbeddedImage('../images/BalTropical_logo_mail.png', 'logo_bt');
        $mail->msgHTML($message, __DIR__);
        if (!isset($_GET['view'])) {
            if ($mail->send()) {
                $log->write("Mail is SUCCESFULLY sent to " . htmlspecialchars($order->users[0]->email), 'success');
            } else {
                $log->write("Mail has FAILED to send " . htmlspecialchars($order->users[0]->email), 'failure');
            }
        } else {
                $log->write("Mail was not send due to view-mode only " . htmlspecialchars($order->users[0]->email), 'failure');
        }
        */
    }


