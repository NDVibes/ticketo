<?php
    //Include
    include_once('config/config.php');
    include_once('classes/utils.php');
    include_once('classes/User.php');
    include_once('classes/Order.php');
    
    //Starting session
    session_start();
    
    //Debug
    ini_set('display_errors', DEBUG);
    
    //Check if is valid on page
    
    if (isset($_SESSION['order'])) {
        if (!$_SESSION['order']->isPaid())
            header('Location: index.php');
    }
        
    //Destroy session    
    session_destroy();
