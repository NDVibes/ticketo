<?php
ini_set('display_errors', 1);

/*
    Class for implementing Nielson. Nielson is a variant of JSON
    but optimized and simplified to used in this ticketingsystem's logs
    (considering PHP dynamic types).

    Note:
        - Nielson syntax is dependent on newlines
        - Array opening bracket should be on same line as property-key
        - All property values should be placed in quotation-marks
        - Type declaration should be done by the implementing software


    Example file:
    ------------------------------------------
    //One instance of a Nielson object
    [ 
        @propertyA "hiwhiewdo"  //Nielson property (string)
        @propertyB "jfhwio"     //Nielson property (string)
        @propertyC "5"          //Nielson property (string/int)
        @propertyD {            //Nielson property (array)
            "A"                 //PropertyD[0]
            "B"                 //PropertyD[1]
            "C"                 //PropertyD[2]
            "D"                 //PropertyD[3]
        }
    ]
        
    ------------------------------------------
 */

class Nielson {
    //Constructor
    //:param file: File to read/write from in Nielson format
    public function __construct($file) {
        $this->file = $file;
    }

    //Methods
    //Method to add object to Nielson instance
    //:param object: NielsonObject to add
    public function add($object) {
        $this->objects[] = $object;
    }

    //ToString method to get string representation of this Nielson instance
    //:return string: String representation of this Nielson instance
    public function __toString() {
        $str = "";
        foreach ($this->objects as $object) {
            $str .= (string)$object;
        }

        return $str;
    }

    //Method to read in Nielson file to memory
    public function read() {
        //Opening file
        $file = new SplFileObject($this->file);
        if (file_exists($this->file)) {
            //Looping over all lines in file
            $lineCounter = 1;
            $startNewObject = true;
            //while (($line = fgets($file)) !== false) {
            while (!$file->eof()) {
                $regex = '\s+(?=((\\[\\"]|[^\\"])*"(\\[\\"]|[^\\"])*")*(\\[\\"]|[^\\"])*$)';
                $line = $file->current();
                $line = preg_replace('/'.$regex.'/', '', $line);

                if ($startNewObject)
                    $NielsonObject =  new NielsonObject(array());

                if ($line == "[") {
                    //Create NielsonObject
                    $startNewObject = false;
                } else if ($line == "]") {
                    //Add NielsonObject
                    $this->objects[] = $NielsonObject;
                    $startNewObject = true;
                } else if (substr($line, 0, 1) == "@") {
                    //Add property
                    if ($NielsonObject !== NULL) {
                        $i = 1;
                        $propertyKey = "";
                        while($line[$i] != "\"" && $line[$i] != "{") {
                            $propertyKey .= $line[$i];
                            $i++;
                        }

                        $propertyValue = NULL;
                        if ($line[strlen($propertyKey) +1] == "{") {
                            //Value will be an array
                            $propertyValue = array();

                            $file->next();
                            while (preg_replace('/'.$regex.'/', '', $file->current()) != "}") {
                                $arrayValue = preg_replace('/'.$regex.'/', '', $file->current());
                                
                                $propertyValue[] = substr($arrayValue, 1, strlen($arrayValue) -2);

                                $file->next();
                            }
                        } else {
                            //Value will be normal (interpreted as string)
                            $propertyValue = "";
                            $i++;
                            while ($line[$i] != "\"") {
                                $propertyValue .= $line[$i];
                                $i++;
                            }
                        }

                        //Add property to object
                        $property = new NielsonProperty($propertyKey, $propertyValue);
                        $NielsonObject->add($property);

                    } else {
                        echo "Nielson Error: Tried to add Nielson Property to non-existing Nielson Object at line " . $lineCounter . ".";
                        return;
                    }
                }

                $lineCounter++;
                $file->next();
            }
        } else {
            echo "Nielson Error: File not found.";
            return;
        }
    }

    //Method to update Nielson-file with changes made in Nielson instance
    //:return bool: Returns true if updating was succesful, false on failure
    public function update() {
        $file = fopen($this->file, 'w');
        if ($file) {
            $string = "";
            if ($this->hide_content) 
                $string = '<?php $HIDE_NIELSON_CONTENT = \' ' . PHP_EOL . (string)$this . PHP_EOL . '\';';
            else
                $string = (string)$this;
            
            fwrite($file, $string);
        } else
            return false;

        fclose($file);
        return true;
    }

    //Method to select Nielson Object by a specified property's value
    //:param propertyKey: key to search at
    //:param propertyValue: Value to match key
    //:param strict: Ignores uppercase/lowercase on true
    //:return NielsonObject: Object value matching value with it's key
    public function getObject($propertyKey, $propertyValue, $strict = false) {
        foreach ($this->objects as $o) {
            $valueInFile = $o->getProperty($propertyKey)->value;
            
            if (!$strict) {
                $valueInFile = strtolower($valueInFile);
                $propertyValue = strtolower($propertyValue);
            }
            
            if ($valueInFile == $propertyValue) 
                return $o;
        }

        return NULL;
    }

    
    //Method to delete Nielson Object matching property-key and property-value
    //:param propertyKey: key to delete
    //:param propertyValue: Value to match key
    public function deleteObject($propertyKey, $propertyValue) {
        $i = 0;
        foreach ($this->objects as $o) {
            if ($o->getProperty($propertyKey)->value == $propertyValue) {
                unset($this->objects[$i]);
                return true;
            }
            $i++;
        }

        return false;
    }

    //Method to get next value of auto increment property-key
    //:param key: Key-name with auto-increment 
    //:return int: Highest value of key-property +1
    function auto_increment($key) {
        $i = 0;
        foreach ($this->objects as $o) {
            if ($o->getProperty($key)->value > $i)
                $i = $o->getProperty($key)->value;
        }

        return $i + 1;
    }

    //Properties
    public $file = "";
    public $objects = array();      //Array holding all objects of this Nielson instance
    public $hide_content = false;   //Boolean, hides content using PHP if true
}

/*
    Class holding NielsonObject design
*/
class NielsonObject {
    //Constructor
    //:param properties: KeyValue Array holding initial
    //properties with initial values
    public function __construct($properties) {
        $this->properties = $properties;
    }

    //Methods
    //Function to print out this object as as string
    //:return string: This object in string-format
    public function __toString() {
        $str = "[\n";
        foreach ($this->properties as $p) {
            $str .= (string)$p;
        }

        $str .= "]\n";
        
        return $str;
    }

    //Method to add Nielson Property to this Nielson Object
    //:param propertie: Nielson property to add
    public function add($property) {
        $this->properties[] = $property;
    }

    //Method to return Nielson Property by property-key
    //:param key: Key of property to return
    //:return NielsonProperty: Property with specified key
    public function getProperty($key) {
        foreach ($this->properties as $p) {
            if ($p->key == $key)
                return $p;
        }

        return NULL;
    }

    //Properties
    public $properties = array();     //Array holding all properties of this object
}

/*
    Class holding NielsonProperty design
 */
class NielsonProperty {
    //Constructor
    //:param key: Property key/name
    //:param value: Value of property
    public function __construct($key, $value) {
        $this->key = $key;
        $this->value = $value;
    }

    //ToString method to give string representation of this property
    //:return str: << @key "value" >>
    public function __toString() {
        if (!is_array($this->value))
            return "\t@" . $this->key . " " . "\"" . $this->value . "\"\n";
        else {
            $str = "\t@" . $this->key . " {\n";
            foreach ($this->value as $value) {
                $str .= "\t\t\"" . $value . "\"\n";
            }
            $str .= "\t}\n";

            return $str;
        }

        return "";
    }

    //Properties
    public $key = "";
    public $value = ""; 
}
