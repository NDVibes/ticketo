<?php
    //Include
    include_once('config/config.php');
    include_once('classes/utils.php');
    include_once('classes/User.php');
    include_once("classes/Order.php");
    include_once("classes/Database.php");
    include_once('classes/Log.php');
    include_once("php/phpqrcode/qrlib.php");
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    require 'php/phpmailer/Exception.php';
    require 'php/phpmailer/PHPMailer.php';
    require 'php/phpmailer/SMTP.php';

    //Starting session
    session_start();

    ini_set('display_errors', DEBUG);

    //Logging
    $log = $_SESSION['log'];
        
    //Create Mollie-object
    $mollie = new \Mollie\Api\MollieApiClient();
    $mollie->setApiKey(MOLLIE_KEY);

    //If there is response from Mollie-webhook
    if (isset($_POST['id'])) {
        //Make database object
        $db = new Database();
        
        $log2 = Log::withPaymentID($_POST['id']);
        $log2->username = $db->select($_POST['id'], 'firstname') . " " . $db->select($_POST['id'], 'lastname');
        $log2->write("Got response from Mollie with payment-id " . htmlspecialchars($_POST['id']));
        
        //Get payment
        $payment = $mollie->payments->get($_POST['id']);
        
        //Check if payment is paid
        if ($payment->isPaid()) {
            $db2 = new Database();
            $stmt = $db2->conn->prepare("SELECT ID, firstname, lastname FROM orders WHERE PaymentID=?");
            $stmt->bind_param("s", $paymentid_);
            $paymentid_ = $_POST['id'];
            $stmt->bind_result($ID, $firstname, $lastname);
            $stmt->execute();

            while ($stmt->fetch()) {

                //Alter database:
                // - Add secret hash
                // - Set paid to true (paid = true)
                // - Set date of order
                // - Set time of order
                $db->update((int)$ID, array(
                    'hash' => generateHash($firstname . $lastname, $_POST['id']),
                    'paid' => 1,
                    'date' => date('Y-m-d'),
                    'time' => date('H:i:s')
                ));

            }
            $stmt->close();

            //Decrease coupon by one
            $stmt = $db->conn->prepare("SELECT coupon FROM orders WHERE PaymentID=?");
            $stmt->bind_param("s", $paymentID_);
            $paymentID_ = $_POST['id'];
            $stmt->bind_result($dbcoupon);
            $stmt->execute();
            $stmt->fetch();
            $coupon = $dbcoupon;
            $stmt->close();

            if ($coupon != "") {
                $stmt = $db->conn->prepare("UPDATE coupons SET available_uses = available_uses - 1 WHERE coupon=? and available_uses != 'INFINITE'");
                $stmt->bind_param("s", $coupon_);
                $coupon_ = $coupon;
                $stmt->execute();

                if ($stmt->error) {
                    $log2->write($_POST['id'] . " Failed to use coupon " . $coupon);
                } else {
                    $log2->write($_POST['id'] . " Succesfully used coupon " . $coupon);
                }
                $stmt->close();
            } 

            $log2->write("Payment with Payment-ID " . $_POST['id'] . " is SUCCESFULLY paid", 'success');
        } else {
            $log2->write("Payment with Payment-ID " . $_POST['id'] . " is NOT paid", 'failure');
        }
    }
    
    //If pay-button is clicked
    else if (isset($_POST['btnPay'])) {
        $log->write("Payment-button clicked on betaling.php");

        //Create order-object
        $order = new Order();

        //Link user to order
        foreach ($_SESSION['users'] as $user) {
            $order->users[] = $user;
        }
        $log->username = $order->users[0]->name();
        
        $log->write("Order created");

        //Set order properies
        $order->coupon = $order->users[0]->coupon;
        $order->price = getPrice();
        $order->description = "Bal Tropical Ticket (incl. Transactiekosten)";
        $order->redirect = HOST . "/betaling.php";
        $order->webhook = HOST . "/betaling.php";
        $order->date = date('d-M-Y');
        $order->time = date('H:i:s');
        $order->paid = 0;

        //Add order to database (paid = false)
        $db = new Database();
        
        $db->insert($order);
        $order->ids = $db->lastID;
        
        $db->close();
        
        //Execute order
        $_SESSION['log']->id = $order->ids[0];
        $log->write("Order-execute function launched", 'important');
        $order->execute($mollie);
        die();
    } 
    
    //If nothing is clicked
    else {
        //Get order
        if (isset($_SESSION['order'])) {
            
            //Add ID to order class
            $db = new Database();
            $order = $_SESSION['order'];
            
            $_SESSION['log']->paymentID = $order->paymentID;

            //Check if order is paid
            if ($order->isPaid()) {
                $log->write("Order is succesfully paid, redirecting to ticket.php ...", 'success');
           
                //Get hash
                $stmt = $db->conn->prepare("SELECT hash FROM orders WHERE ID=?");
                $stmt->bind_param("i", $id_);
                $id_ = $order->ids[0];
                $stmt->bind_result($hash);
                $stmt->execute();
                $stmt->fetch();
                $hash_first_user = $hash;
                $stmt->close();
            
                //Send ticket to everyone by mail
                if(sendMail($order)) {
                    //Redirect
                    header('Location: ticket.php?hash=' . $hash_first_user);
                    die();
                }

            } else {
                $log->write("Order is not paid on betaling.php, redirecting to error-page", 'failure');
                header('Location: error.php');
                die();
            }
        } else {
            if (!isset($_SESSION['users'])) {
                //User should not be on this page
                $log->write("No user-session found on betaling.php, redirecting to error page ...", 'failure');
                header('Location: error.php');
                die();
            }
        }
    }

    function getPrice() {
        //Get price with coupon
        $db = new Database();
        $discount = 0;
        $stmt = $db->conn->prepare("SELECT discount FROM coupons WHERE UPPER(coupon)=? AND (available_uses > 0 OR available_uses = 'INFINITE')");
        $stmt->bind_param("s", $coupon_);
        $coupon_ = strtoupper($_SESSION['users'][0]->coupon);
        $stmt->bind_result($db_discount);
        $stmt->execute();
        $stmt->fetch();
        $discount = (int)$db_discount;
        $stmt->close();
        
        $price_str = (string)number_format((PRICE * sizeof($_SESSION['users'])) - (PRICE * ($discount / 100)), 2, '.', '');
        if ((int)$price_str < 1) {
            $price_str = (string)((int)$price_str * sizeof($_SESSION['users']));
        } 
        
        return $price_str;
    }

    //Function to send mail and generate QR for all users
    function sendMail($order) {
        $i = 0;
        foreach ($order->users as $user) {

            $ID = $order->ids[$i];

            $log = new Log($ID, $order->users[$i]->firstname . " " . $order->users[$i]->lastname, $order->users[$i]->email);

            $db = new Database();
            $stmt = $db->conn->prepare("SELECT hash FROM orders WHERE ID=?");
            $stmt->bind_param("i", $ID_);
            $ID_ = $ID;
            $stmt->bind_result($dbhash);
            $stmt->execute();
            $stmt->fetch();
            $hash = $dbhash;
            $stmt->close();
            

            //Data for QRCode
            $str = $ID . "," . $hash;
            $path = "qrcodes/" . $ID . "_" . $order->paymentID . ".png";
        
            //Generate QRCode(ID,hash)
            QRCode::png($str, $path, QR_ECLEVEL_L, 2);
            
            //Generate pseudo-code
            $pseudocode = pseudoCode($user, $ID);
            
            //Send mail
            $url = HOST . "/ticket.php?hash=" . $hash . "&view=true";
            $message = file_get_contents("partials/mail.html");
            $message = str_replace("%firstname%", $order->users[$i]->firstname, $message);
            $message = str_replace("%url%", $url, $message);
            
            $mail = new PHPMailer();
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = MAIL_HOST;
            $mail->Port = MAIL_PORT;
            $mail->SMTPAuth = true;
            $mail->Username = MAIL_USER;
            $mail->Password = MAIL_PASS;
            $mail->setFrom(MAIL_FROM, 'Bal Tropical Tickets');
            $mail->addAddress($order->users[$i]->email, $order->users[$i]->firstname . " " . $order->users[$i]->lastname);
            $mail->Subject = 'Uw Bal Tropical ticket';
            $mail->AddEmbeddedImage('../images/BalTropical_logo_mail.png', 'logo_bt');
            $mail->msgHTML($message, __DIR__);
            if (!isset($_GET['view'])) {
                if ($mail->send()) {
                    $log->write("Mail is SUCCESFULLY sent to " . htmlspecialchars($order->users[$i]->email), 'success');
                } else {
                    $log->write("Mail has FAILED to send " . htmlspecialchars($order->users[$i]->email), 'failure');
                }
            } else {
                    $log->write("Mail was not send due to view-mode only " . htmlspecialchars($order->users[$i]->email), 'failure');
            }
            $mail->smtpClose();
            $i++;
        }

        return true;
    }
