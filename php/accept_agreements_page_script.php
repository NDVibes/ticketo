<?php
    //Include
    include_once('config/config.php');
    include_once('classes/utils.php');
    include_once('classes/Database.php');
    
    //Starting session
    session_start();
    
    //Debug
    ini_set('display_errors', DEBUG);

    if (!isset($_GET['hash'])) {
        header('Location: error.php');
        die();
    } else {
        $db = new Database();
        $stmt = $db->conn->prepare("SELECT birthdate FROM orders WHERE hash=?");
        $stmt->bind_param("s", $hash_);
        $hash_ = $_GET['hash'];
        $stmt->bind_result($db_birthdate);
        $stmt->execute();
        $stmt->fetch();
        $birthdate = $db_birthdate;
        $stmt->close();
        
        echo "<script>window.onload = function () {displayMinSixteen('".date('D M d Y H:i:s O', strtotime($birthdate))."'); }</script>";
    }

    if (isset($_POST['btnAccept'])) {
        $accepted = $_POST['accepted1'] && $_POST['accepted2'] && $_POST['accepted3'];

        if ($accepted) {
            $db = new Database();
            $stmt = $db->conn->prepare("UPDATE orders SET accepted_agreements=1 WHERE hash=?");
            $stmt->bind_param("s", $hash_);
            $hash_ = $_GET['hash'];
            $stmt->execute();
            $stmt->close();

            header('Location: ticket.php?hash=' . $_GET['hash']);
        }
    }
