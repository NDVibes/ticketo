<?php
    //Include
    include_once('config/config.php');
    include_once('classes/utils.php');
    include_once('classes/User.php');
    include_once('classes/Order.php');
    include_once('classes/Database.php');
    include_once('classes/Log.php');
    include_once('php/phpqrcode/qrlib.php');

    //Starting session
    session_start();
    
    //Debug mode
    ini_set('display_errors', DEBUG);
    
    //Check if user is allowed on ticket.php
    if (!isset($_SESSION['order'])) {
        header('Location: error.php');
    }
    
    //Get order
    $order = $_SESSION['order'];
    
    $log = $_SESSION['log'];
    $log->write("Printed out his ticket");
    
    //User must have paid to continue
    if (!$order->isPaid()) {
        header('Location: error.php');
    }
    
    //Get ID and hash from database with paymentID
    $db = new Database();
    $ID = $db->select($order->paymentID, 'ID');
    $hash = $db->select($order->paymentID, 'hash');
    
    //Data for QRCode
    $str = $ID . "," . $hash;
    $path = "qrcodes/" . $ID . "_" . $order->paymentID . ".png";
    
