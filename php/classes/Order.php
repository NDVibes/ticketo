<?php
    //Include the Mollie-API
    include_once("php/mollie-api-php/vendor/autoload.php");
    include_once("User.php");
    include_once('Database.php');
    include_once('config/config.php');

    ini_set('display_errors', DEBUG);

    //PHP-file for Order-class
    class Order {
        //Constructor
        function __construct() {
            $this->price = "";
            $this->description = "";
            $this->redirect = "";
            $this->date = "";
            $this->coupon = "";
            $this->time = "";
            $this->users = array();
        }
        
        //Function to execute the payment
        //:return bool: Returns false on failure
        public function execute($mollie) {
            //Check if order is valid
            if (!$this->isValid())
                return false;

            //Payment properties
            $payment = $mollie->payments->create([
                "amount" => [
                    "currency" => "EUR",
                    "value" => $this->price
                ],

                "description" => $this->description,
                "redirectUrl" => $this->redirect,
                "webhookUrl" => $this->webhook
            ]);

            //Add paymentID to class
            $this->paymentID = $payment->id;

            //Update paymentID in database
            $db = new Database();
            $db->setTable('orders');

            foreach ($this->ids as $id) {
                $db->update($id, array(
                    'PaymentID' => $payment->id
                ));
            }

            //Save order to SESSION
            $_SESSION['order'] = $this;

            //Goto Mollie-checkout page
            header("Location: " . $payment->getCheckoutUrl(), true, 303);
            return true;
        }

        //Function to check if the Order is valid
        //:return bool: Returns true if succesful
        public function isValid() {
            //First user should be valid
            foreach ($this->users as $user) {
                if (!$user->isValid())
                    return false;
            }
            

            //Check properties
            if ($this->price <= 0)
                return false;
            if ($this->description == "" ||
                $this->redirect == "" ||
                $this->date == "" ||
                $this->time == "" ||
                sizeof($this->users) == 0) {
                    return false;
                }

            return true;
        }
        
        //Function to check if order isPaid
        //:return bool: Returns true if paid
        public function isPaid() {
            $db = new Database();
            
            $stmt = $db->conn->prepare("SELECT paid FROM orders WHERE PaymentID=?");
            $stmt->bind_param("s", $PaymentID_);
            $PaymentID_ = $this->paymentID;
            $stmt->bind_result($paid);
            $stmt->execute();

            while($stmt->fetch()) {
                if ($paid == 0) {
                    return false;
                }
            }

            return true;
        }
    
        //Properties
        public $ids = array();
        public $paymentID = "";
        public $price = "";
        public $coupon = "";
        public $description = "";
        public $webhook = "";
        public $redirect = "";
        public $date = "";
        public $time = "";
        public $users = array();
        public $mollie = NULL;
        public $paid = 0;
        
    }
