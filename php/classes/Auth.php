<?php

    include_once('config/config.php');
    include_once('php/classes/Log.php');
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    //Class for Auth-class
    class Auth {
        //Constructor
        public function __construct() {
        }

        //Function to prompt for password
        public function prompt() {        
            if (!$this->validated) {
                header('Location: ' . $this->authUrl);
            } else {
                $adminlog = new AdminLog();
                $adminlog->write($this->user, "Accessed " . $this->ret);
            }
        }
        
        //Function to check if password is validated
        //:param user: User to validate
        //:param pass: Password to validate
        public function check($user, $pass) {
            $conn = new mysqli(DBHOST, USER, PASS, DTBS);
            $stmt = $conn->prepare("SELECT password, permission FROM admins WHERE admin=?");
            $stmt->bind_param("s", $admin_);
            $admin_ = $user;
            $stmt->bind_result($passwordFromDB, $permission);
            $stmt->execute();
            $stmt->fetch();

            $adminlog = new AdminLog();
            if ($permission >= $this->neededPermission && password_verify($pass, $passwordFromDB)) {
                $this->validated = true;
                $this->permission = $permission;
                $this->user = $user;
                $adminlog->write($user, "Logged in at " . $this->ret);

                //Set authorization cookie
                $auth_str = bin2hex(openssl_random_pseudo_bytes(16, $strong));
                setcookie('AJAX_AUTH', $auth_str);

                $auth_cookie_file = PATH ."/admin/AUTH_COOKIES/authcookie.txt.php";
                if (!file_exists($auth_cookie_file)) {
                    $file = fopen($auth_cookie_file , "a");
                    fwrite($file, '<?php $HIDE_CONTENT = "' . PHP_EOL . ":");
                    fclose($file);
                }

                $file = fopen($auth_cookie_file , "a");
                fwrite($file, $auth_str . ":");
                fclose($file);

                $this->auth_str = $auth_str;

                header('Location: ' . $this->ret);
            } else {
                $syslog = new SystemLog(PATH . "/admin/systemlogs/systemlog.nielson.php");
                $syslog->write("Failed attempt to authorize for admin " . $user);
                echo "Credentials invalid or you don't have the right permissions...";
                return;
            }
        }
        
        
        //Properties
        public $user = '';
        public $validated = false;
        public $ret = '';
        public $authUrl = "";
        public $neededPermission = "";
        public $permission = 0;
        public $auth_str = "";
    }
