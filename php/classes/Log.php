<?php
    //PHP-file for Log-class
    include_once('config/config.php');
    include_once('php/Nielson/Nielson.php');
    include_once('Database.php');
    ini_set('display_errors', DEBUG);


    class Log {
        
        //Constructor
        //:param id: ID to log
        //:param username: Firstname + Lastname of user to log
        public function __construct($id, $username, $email) {
            $this->id = $id;
            $this->username = $username;
            $this->email = $email;
            $this->syslog = new SystemLog(PATH . "/admin/systemlogs/systemlog.nielson.php");
        }

        //Constructor with paymentID
        //:param paymentID: PaymentID to link logs to
        public function withPaymentID($paymentID) {
            $db = new Database();

            $id = $db->select($paymentID, 'id');
            $username = $db->select($paymentID, 'firstname') . " " . $db->select($paymentID, 'lastname');
            $email = $db->select($paymentID, 'email');

            $log = new self($id, $username, $email);
            $log->paymentID = $paymentID;

            return $log;
        }
        
        //Methods
        //Function to write
        //:param msg: Message to log
        //:param class: CSS-class of message (optional)
        public function write($msg, $class = '') {

            $db = new mysqli(DBHOST, USER, PASS, DTBS);
            if ($db->connect_error) {
                $this->syslog->write("Error connecting to database in Log.php: " . $db->connect_error);
                return false;
            }
            $stmt = $db->prepare("INSERT INTO logs (userID, PaymentID, username, email, msg, type, datetime) VALUES (?, ?, ?, ?, ?, ?, ?)");
            if ($stmt === false) {
                $this->syslog->write("Error in query in Log.php: " . $db->error);
                return;
            }

            $stmt->bind_param("issssss", $userID_, $paymentID_, $username_, $email_, $msg_, $type_, $datetime_);

            $userID_ = $this->id;
            $paymentID_ = $this->paymentID;
            $username_ = $this->username;
            $email_ = $this->email;
            $msg_ = $msg;
            $type_ = $class;
            $datetime_ = date('Y-m-d H:i:s');

            $stmt->execute();
            $this->insertID[] = $db->insert_id;
            if ($stmt->error) {
                $this->syslog->write("Error writing to log in Log.php: " . $stmt->error);
                return;
            }
            $stmt->close();

            if ($this->id != 0 || $this->paymentID != "") {
                $str = "";
                foreach ($this->insertID as $ID) {
                    if (!is_int($ID))
                        return;
                    $str .= $ID . ",";
                }
                $str = substr($str, 0, strlen($str)-1);

                $stmt = $db->prepare("UPDATE logs SET userID=?, paymentID=? WHERE ID IN(".$str.")");
                $stmt->bind_param('is', $userID_, $paymentID_);
                $userID_ = $this->id;
                $paymentID_ = $this->paymentID;
                $stmt->execute();

                if ($stmt->error) {
                    $this->syslog->write("Error updating log in Log.php: " . $stmt->error);
                    return;
                }
                $stmt->close();
            }

            if ($this->paymentID != "") {
                $stmt = $db->prepare("UPDATE logs SET userID=? where PaymentID=?");
                $stmt->bind_param('is', $userID_, $paymentID_);
                $userID_ = $this->id;
                $paymentID_ = $this->paymentID;
                $stmt->execute();

                if ($stmt->error) {
                    $this->syslog->write("Error updating log in Log.php: " . $stmt->error);
                    return;
                }
                $stmt->close();
            
            }
        }
        
        //Properties
        public $username = "";
        public $paymentID = "";
        public $email = "";
        public $id = 0;
        public $db = NULL;
        public $insertID = array();
        public $syslog = NULL;
    }

    class SystemLog {
        public function __construct($file) {
            $this->file = $file;
            $this->nielson = new Nielson($this->file);
            $this->nielson->read();
        }

        public function write($msg) {
            $object = new NielsonObject(
                array(
                    new NielsonProperty("ID", $this->nielson->auto_increment("ID")),
                    new NielsonProperty("datetime", date('Y-m-d H:i:s')),
                    new NielsonProperty("msg", $msg)
                )
            );
            $this->nielson->add($object);
            $this->nielson->update();
        }

        public function __toString() {
            $str = "";
            foreach ($this->nielson->objects as $o) {
                $str .= "<b>" . $o->getProperty("ID")->value . " (" . $o->getProperty("datetime")->value . ") - </b>" . $o->getProperty("msg")->value . "<br>";
            }

            return $str;
        }

        //Properties
        public $file = PATH . "/admin/systemlogs/systemlog.nielson.php";
        public $nielson = NULL;
    }

    class AdminLog {
        public function __construct() {
            $this->db = new mysqli(DBHOST, USER, PASS, DTBS);
            $this->syslog = new SystemLog(PATH . "/admin/systemlogs/systemlog.nielson.php");
        }

        function write($admin, $msg) {
            $stmt = $this->db->prepare("INSERT INTO adminlogs (admin, msg, datetime) VALUES (?, ?, ?)");
            $stmt->bind_param("sss", $admin_, $msg_, $datetime_);
            $admin_ = $admin;
            $msg_ = $msg;
            $datetime_ = date('Y-m-d H:i:s');
            
            $stmt->execute();
            if ($stmt->error) {
                $this->syslog->write("Error writing to adminlogs in Log.php");
                return;
            }

            $stmt->close();
        }

        public function __toString() {
            $str = "";

            $stmt = $this->db->prepare("SELECT admin, msg, datetime FROM adminlogs ORDER BY datetime DESC");
            $stmt->bind_result($admin, $msg, $datetime);
            $stmt->execute();

            while($stmt->fetch()) {
                $str .= "<b>[" . $datetime . "]</b> " . $admin . " " . $msg . "<br>";
            }

            
            $stmt->close();
            return $str;
        }

        //Properties
        public $db = NULL;
        public $syslog = NULL;
    }
