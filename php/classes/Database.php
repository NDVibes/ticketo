<?php
    ini_set('display_errors', 1);

    //Include
    include_once('config/config.php');
    include_once('php/classes/Order.php');
    include_once('php/classes/Log.php');


    //PHP-file for Database class
    class Database {
        //Constructor
        public function __construct() {
            $this->syslog = new SystemLog(PATH . "/admin/systemlogs/systemlog.nielson.php");

            $this->conn = new mysqli(DBHOST, USER, PASS, DTBS);
            if ($this->conn->connect_error) {
                $this->syslog->write("Error connecting to database in Database.php: " . $this->conn->connect_error);
                return false;
            } else {
                return true;
            }
        }

        //Function to set table
        //:param table: Name of table to work in
        //:return bool: Returns true if succesful
        public function setTable($table) { 
            //Check if table is allowed
            if (!in_array($this->table, $this->allowed_tables)) {
                return false;
            }

            $this->table = $table;
            return true;
        }

        //Function to insert objects in database
        //:param object: Object to insert into database
        //:return bool: Returns true if inserted succesful
        public function insert($object) {
            //If object is an Order
            if (is_a($object, 'Order')) {
                $this->table = 'orders';
                //Check if table is allowed
                if (!in_array($this->table, $this->allowed_tables)) {
                    return false;
                }
                
                //Insert to database
                $stmt = $this->conn->prepare("INSERT INTO ". htmlspecialchars($this->table) ." (PaymentID, firstname, lastname, email, birthdate, accepted_agreements, paid, scanned, date, coupon, time, hash) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

                if ($stmt === false) {
                    $this->syslog->write("Error in SQL insert query in Database.php: " . $this->conn->db_error);
                    return;
                }


                $stmt->bind_param("sssssiiissss", $paymentID_, $firstname_, $lastname_, $email_, $birthdate_, $accepted_agreements_, $paid_, $scanned_, $date_, $coupon_, $time_, $hash_);
                $table_ = $this->table;
                foreach ($object->users as $user) {
                    $paymentID_ = $object->paymentID;
                    $firstname_ = $user->firstname;
                    $lastname_ = $user->lastname;
                    $email_ = $user->email;
                    $birthdate_ = $user->birthdate;
                    $accepted_agreements_ = $user->accepted;
                    $paid_ = $object->paid;
                    $scanned_ = 0;
                    $coupon_ = $object->coupon;
                    $date_ = date('Y-m-d');
                    $time_ = date('H:i:s');
                    $hash_ = "";

                    $stmt->execute();
                    if ($stmt->error) {
                        $this->syslog->write("Error in SQL insert query in Database.php: " . $stmt->error);
                        return;
                    }
                    $this->lastID[] = $this->conn->insert_id;
                }

                $stmt->close();


                return true;
            } else if (is_array($object)) {
                /*
                if (!in_array($this->table, $this->allowed_tables))
                    return false;
                
                $toInsert = $object;
                $cols = "";
                $qmarks = "";
                $type = "";
                foreach ($toInsert as $column => $value) {
                    if (!in_array($column, $this->allowed_columns))
                        return false;

                    $cols .= $column . ",";
                    $qmarks .= "?,";
                    if (is_int($value))
                        $s .= "i";
                    else if (is_string($value))
                        $s .= "s";
                    else
                        $s .= "s";
                }

                $cols = substr($cols, 0, strlen($cols)-1);
                $qmarks = substr($qmarks, 0, strlen($qmarks)-1);

                $sql = "INSERT INTO " . $this->table . " (".$cols.") VALUES (".$qmarks.")";
                $stmt->bind_param("");*/
            }
            
            return false;
        }

        //Function to retrieve Order from database
        //by ID (int) or PaymentID (string)
        //:param id: ID (int) or PaymentID (string) of object to retrieve
        //:return Order: Returns the order matching id
        public function get($id) {
            $stmt = null;
            if (is_int($id)) {
                //If param is int -> retrieve by ID
                $stmt = $this->conn->prepare("SELECT ID, PaymentID, firstname, lastname, email, birthdate, accepted_agreements, paid, scanned FROM orders WHERE ID=? ORDER BY ID ASC");


                $stmt->bind_param("i", $ID_);
                $ID_ = $id;
            } else if (is_string($id)) {
                //Else if param is string -> retrieve by PaymentID
                $stmt = $this->conn->prepare("SELECT ID, PaymentID, firstname, lastname, email, birthdate, accepted_agreements, paid, scanned FROM orders WHERE PaymentID=? ORDER BY ID ASC");
                $stmt->bind_param("s", $ID_);
            } else {
                //Else return false
                return false;
            }
           
            //Get from database
            $stmt->execute();
            if ($stmt->error) {
                $this->syslog->write("Error in SQL get query in Database.php: " . $stmt->error);
                return;
            }
            $stmt->bind_result($ID, $paymentID, $firstname, $lastname, $email, $birthdate, $accepted_agreements, $paid, $scanned);
            
            //Create Order-object with data from database
            $order = new Order();
            while ($stmt->fetch()) {
                $user = new User();
                $user->firstname = $firstname;
                $user->lastname = $lastname;
                $user->email = $email;
                $user->birthdate = $birthdate;
                $user->accepted = $accepted_agreements;
                
                $order->ids[] = $ID;
                $order->users[] = $user;
            }

            $order->paymentID = $paymentID;
            $order->paid = $paid;
            
            $stmt->close();
            return $order;
        }

        //Function to delete from database by ID (int) or PaymentID (string)
        //:param id: ID (int) or PaymentID (string) of object to delete
        //:return bool: Returns true if succesful
        public function delete($id) {
            $stmt = $this->conn->prepare("DELETE FROM ".htmlspecialchars($this->table)." WHERE ID=?");
            if (is_int($id)) {
                //If param is int -> delete by ID
                $stmt = $this->conn->prepare("DELETE FROM ".htmlspecialchars($this->table)." WHERE ID=?");
                $stmt->bind_param("i", $ID_);
                $ID_ = $id;
            } else if (is_string($id)) {
                //If param is string -> delete by PaymentID
                $stmt = $this->conn->prepare("DELETE FROM orders WHERE PaymentID=?");
                $stmt->bind_param("s", $ID_);
                $ID_ = $id;
            } else {
                //Else return false
                return false;
            }


            //Delete from database
            $stmt->execute();
            if ($stmt->error) {
                $this->syslog->write("Error in SQL delete query in Database.php: " . $stmt->error);
                return;
            }
            
            $stmt->close();

            return true;
        }

        //Function to update/change data in database
        public function update($id, $data) {
            
            
            //Query
            $query = "UPDATE " . $this->table . " SET ";

            //Loop over array (data requiring update and new value)
            foreach ($data as $key => $value) {
                $query .= $key . " = '" . $value . "',";
            }

            //Delete last comma in query-string
            $query = substr($query, 0, -1);

            //Query condition
            $bind = "i";
            if (is_int($id)) {
                $query .= " WHERE ID=?";
                $bind = "i";
            } else if (is_string($id)) {
                $query .= " WHERE PaymentID=?";
                $bind = "s";
            } else {
                return false;
            }
            
            $stmt = $this->conn->prepare($query);
            $stmt->bind_param($bind, $ID_);
            $ID_ = $id;

            //Update database
            $stmt->execute();
            if ($stmt->error) {
                $this->syslog->write("Error in SQL update query in Database.php: " . $stmt->error);
                return;
            }
            
            $stmt->close();
            
            return true;
        }

        //Function to get the lastID
        //:return int: Latest ID in table
        public function lastID() {
            $result = $this->conn->query("SELECT MAX(ID) FROM orders");
            $row = $result->fetch_assoc();
            return (int)$row['MAX(ID)'];
        }

        

        //Function to close database connection
        //:return bool: Returns true on success
        public function close() {
            $this->conn->close();
            return true;
        }
        
        //Function to select specific column value matching the PaymentID
        //:param id: PaymentID to check for
        //:param column: Column to check value
        //:return value: Value of column matching PaymentID, also false on failure
        public function select($id, $column) {
            //Check if columm is valid
            if (!in_array($column, $this->allowed_columns))
                return false;
        
            $stmt = $this->conn->prepare("SELECT " . $column . " FROM " . $this->table . " WHERE PaymentID=?");
            $stmt->bind_param("s", $ID_);
            $ID_ = $id;
            $stmt->execute();
            if ($stmt->error) {
                $this->syslog->write("Error in SQL select query in Database.php: " . $stmt->error);
                return;
            }
           
            $stmt->bind_result($value);
            $stmt->fetch();
            $rvalue = $value;

            $stmt->close();
            
            return $rvalue;
        }

        public function selectById($id, $column) {
            //Check if columm is valid
            if (!in_array($column, $this->allowed_columns))
                return false;
        
            $stmt = $this->conn->prepare("SELECT " . $column . " FROM " . $this->table . " WHERE ID=?");
            $stmt->bind_param("i", $ID_);
            $ID_ = $id;
            $stmt->execute();
            if ($stmt->error) {
                $this->syslog->write("Error in SQL select query in Database.php: " . $stmt->error);
                return;
            }
            
            $stmt->bind_result($value);
            $stmt->fetch();
            $rvalue = $value;
            $stmt->close();
            
            return $rvalue;
        }
        
        //Function to return all table content
        //:return array[][]: Array containing all table-data
        public function getTable() {
            $result = mysqli_query($this->conn, "SELECT ID, firstname, lastname, email, birthdate, accepted_agreements, paid, scanned, date, PaymentID FROM " . $this->table);
         
            $myTable = array();
            while($row = mysqli_fetch_array($result)) {
                $myTable[] = $row;
            }
            
            return $myTable;
        }
        
        //Function to get amount of rows in table
        //:return int: amount of rows
        public function countRows() {
            $stmt = $this->conn->prepare("SELECT COUNT(ID) FROM " . $this->table);
            $stmt->execute();
            $stmt->bind_result($count);
            $stmt->fetch();
            
            return $count;
        }

        //Properties
        public $conn = "";
        public $table = "orders";
        public $lastID = array();
        public $allowed_tables = array(
            'orders',
            'logs'
        );
        public $allowed_columns = array(
            'ID',
            'PaymentID',
            'paid',
            'scanned',
            'hash',
            'email',
            'firstname',
            'lastname'
        );
        //System log
        public $syslog = NULL; 
    }


