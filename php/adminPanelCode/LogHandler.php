<?php
    include_once('../../config/config.php');
    include_once('php/classes/Auth.php');
    include_once('php/classes/Database.php');
    include_once('php/classes/utils.php');
    session_start();
   
    ini_set('display_errors', DEBUG);
    
    
    //Open database connection
    $db = new Database();
    $db->table = "logs";
 
    //Get filter values
    $filters = explode(';', $_GET['filters']);
    
    $whereStr = " WHERE ";
    //MySQLi escaping filters
    for ($i = 0; $i < sizeof($filters); $i++) {
    
        $filters[$i] = mysqli_real_escape_string($db->conn, $filters[$i]);
        $filters[$i] = str_replace("{", "'", $filters[$i]);
        $filters[$i] = str_replace("}", "'", $filters[$i]);
    }

    foreach ($filters as $filter) {
        $whereStr .= $filter;

        if (next($filters))
            $whereStr .= " AND ";
    }
    if ($whereStr == " WHERE ")
        $whereStr = "";

    //Get group by user or not
    $groupByUserStr = "ORDER BY datetime DESC";
    if (isset($_GET['groupByUser']) && $_GET['groupByUser'] == 1) {
        $groupByUserStr .= ", PaymentID, userID";
    }

    $stmt = $db->conn->prepare("SELECT ID, userID, PaymentID, username, email, msg, type, datetime FROM logs ". $whereStr ." " . $groupByUserStr);
    $stmt->execute();
   
    $stmt->store_result();
    if (isset($_GET['rowAmount'])) {
        if ($_GET['rowAmount'] == $stmt->num_rows) {
            echo "NNR"; //No New Rows
            return;
        }
    }
    
    //Check if request has valid source
    if (!isset($_GET['auth'])) {
        die();
    } else {
        $file = file_get_contents(PATH . "/admin/AUTH_COOKIES/authcookie.txt.php");
        
        if (!in_array($_GET['auth'], explode(":", $file))) {
            die();
        }
    }
    
    $stmt->bind_result($ID, $userID, $paymentID, $username, $email, $msg, $type, $datetime);
            
    while ($stmt->fetch()) {
        $colors = [
            "#FF0000",
            "#00FF00",
            "#0000FF",
            "#00FFFF",
            "#FFC000"
        ];
        $color = $colors[$userID % sizeof($colors)];

        $txt = "<span data-paymentID='" . htmlspecialchars($paymentID) . "' data-username='" . htmlspecialchars($username) . "' data-email='".htmlspecialchars($email)."' class='".htmlspecialchars($type)."'><b><span style='font-weight:bold;color:".htmlspecialchars((string)$color)."'>" . htmlspecialchars($datetime) . "</span> - " .  htmlspecialchars($userID) . " - " . htmlspecialchars($username) . ":</b> " . htmlspecialchars($msg) . "</span><br>"; 
       echo $txt; 
    }
    
    $stmt->close();
