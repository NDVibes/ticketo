<?php
    include_once('../../config/config.php');
    include_once('../classes/Database.php');
    include_once('../classes/utils.php');

    ini_set('display_errors', DEBUG);
    
    //Check if request has valid source
    if (!isset($_POST['auth'])) {
        die();
    } else {
        $file = file_get_contents(PATH . "/admin/AUTH_COOKIES/authcookie.txt.php");
        
        if (!in_array($_POST['auth'], explode(":", $file))) {
           die();
        }
    }

    $db = new mysqli(DBHOST, USER, PASS, DTBS);
    if (isset($_POST['btnAddCoupons'])) {
        $coupon = $_POST['coupon'];
        $discount = $_POST['discount'];
        $available_uses = $_POST['available_uses'];


        foreach ($coupon as $key => $n) {
            $stmt = $db->prepare("INSERT INTO coupons (coupon, discount, available_uses) VALUES (?, ?, ?)");

            $stmt->bind_param("sss", $coupon_, $discount_, $available_uses_);

            $coupon_ = $n;
            $discount_ = $discount[$key];
            $available_uses_ = $available_uses[$key];

            $stmt->execute();

            if ($stmt->error) {
                echo $stmt->error;
            } else {
                echo "Coupons are added succesfully!";
                header('Location: ../../admin/manage-coupons.php');
            }

            $stmt->close();
        }
    }

    if (isset($_POST['btnDeleteCoupon'])) {
        $stmt = $db->prepare("DELETE FROM coupons WHERE ID=?");
        $stmt->bind_param("i", $ID_);
        $ID_ = $_POST['toDelete'];
        $stmt->execute();
            
        if ($stmt->error) {
            echo $stmt->error;
        } else {
            echo "Coupons succesfully deleted!";
            header('Location: ../../admin/manage-coupons.php');
        }
        
        $stmt->close();
    }
