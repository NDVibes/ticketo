<?php

    include_once('../../config/config.php');
    include_once('../classes/Database.php');
    include_once('../classes/utils.php');

    ini_set('display_errors', DEBUG);

    //Check if request has valid source
    if (!isset($_POST['auth'])) {
        die();
    } else {
        $file = file_get_contents(PATH . "/admin/AUTH_COOKIES/authcookie.txt.php");
        
        if (!in_array($_POST['auth'], explode(":", $file))) {
           die();
        }
    }
    
    if (isset($_POST['btnAddUsers'])) {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $birthdate = $_POST['birthdate'];

        print_r($firstname);

        $db = new Database();

        foreach ($firstname as $key => $n) {
            $stmt = $db->conn->prepare("INSERT INTO orders (firstname, lastname, email, birthdate, hash, paid, PaymentID, date, time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("sssssisss", $firstname_, $lastname_, $email_, $birthdate_, $hash_, $paid_, $paymentID_, $date_, $time_);

            $firstname_ = $n;
            $lastname_ = $lastname[$key];
            $email_ = $email[$key];
            $birthdate_ = $birthdate[$key];

            $bytes = openssl_random_pseudo_bytes(50, $cstrong);
            $hex = bin2hex($bytes);
            $hash_ = generateHash($firstname_ . $lastname_, $hex);

            $paid_ = 1;
            $paymentID_ = "ADDED_MANUALLY_" . substr($hash_, 7, 11);

            $date_ = date('Y-m-d');
            $time_ = date('H:i:s');
            $stmt->execute();

            if ($stmt->error) {
                echo $stmt->error;
            } else {
                echo "Users are added succesfully!";
                header('Location: ../../admin/database.php');
            }

            $stmt->close();
        }
    }
