<?php 
    include_once('../../config/config.php');
    include_once('../classes/Database.php');
    
    session_start();
    
    ini_set('display_errors', 1);
    
    //open database
    $db = new Database();

    //Meta
    $total = 0;
    $amountpaid = 0;
    $amountscanned = 0;
                
    //Get filter values
    $filters = explode(';', $_GET['filters']);
    
    $whereStr = " WHERE ";
    //MySQLi escaping filters
    for ($i = 0; $i < sizeof($filters); $i++) {
    
        $filters[$i] = mysqli_real_escape_string($db->conn, $filters[$i]);
        $filters[$i] = str_replace("{", "'", $filters[$i]);
        $filters[$i] = str_replace("}", "'", $filters[$i]);
    }

    foreach ($filters as $filter) {
        $whereStr .= $filter;

        if (next($filters))
            $whereStr .= " AND ";
    }
    if ($whereStr == " WHERE ")
        $whereStr = "";

    //Show table
    $result = mysqli_query($db->conn, "SELECT ID, firstname, lastname, email, birthdate, accepted_agreements, paid, scanned, date, coupon, PaymentID, hash FROM orders " . $whereStr . " ORDER BY ID DESC");

    
    if (isset($_GET['rowAmount'])) {
        if ($_GET['rowAmount'] == mysqli_num_rows($result)) {
            echo "NNR"; //No New Rows
            return;
        }
    }
    
    //Check if request has valid source
    if (!isset($_GET['auth'])) {
        die();
    } else {
        $file = file_get_contents(PATH . "/admin/AUTH_COOKIES/authcookie.txt.php");
        
        if (!in_array($_GET['auth'], explode(":", $file))) {
            die();
        }
    }

    $table = array();
    while($row = mysqli_fetch_array($result)) {
        $table[] = $row;
    }
    
    foreach ($table as $row) {
        echo "<tr>";
        
        $total++;
        
        for ($i = 0; $i <= 11; $i++) {
            if ($row[$i] == '1') {
                if ($i == 6) $amountpaid++;
                if ($i == 7) $amountscanned++;
                if ($i == 5 || $i == 6 || $i == 7) { 
                    echo "<td style='color: green;'>Yes</td>";
                } else {
                    echo '<td>1</td>';
                }
            } else if ($row[$i] == '0') {
                echo "<td style='color: red;'>No</td>";
            } else {
                if ($i == 11) {
                    echo "<td style='text-align:center;'><a target='_blank' href=".HOST."/ticket.php?hash=".$row[$i]."&view=true>View</a></td>";
                } else {
                    echo "<td>".htmlspecialchars($row[$i])."</td>";
                }
            }
        }
        
        echo "</tr>";
    }
    
    echo "<script>document.getElementById('total').innerHTML = " . htmlspecialchars($total) . "</script>";
    echo "<script>document.getElementById('paid').innerHTML = " . htmlspecialchars($amountpaid) . "</script>";
    echo "<script>document.getElementById('scanned').innerHTML = " . htmlspecialchars($amountscanned) . "</script>";
