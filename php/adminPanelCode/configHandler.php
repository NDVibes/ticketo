<?php

    include_once('../../config/config.php');
    include_once('php/Nielson/Nielson.php');

    ini_set('display_errors', DEBUG);

    //Check if request has valid source
    if (!isset($_POST['auth'])) {
        die();
    } else {
        $file = file_get_contents(PATH . "/admin/AUTH_COOKIES/authcookie.txt.php");
        
        if (!in_array($_POST['auth'], explode(":", $file))) {
           die();
        }
    }
   
    //Nielson
    $nielson = new Nielson(PATH . "/config/systemconfig.nielson.php");
    $nielson->hide_content = true;
    $nielson->read();

    //Return Str
    $returnStr = HOST . "/admin/config.php";

    //Debug options
    if (isset($_POST['btnDebug'])) {
        $object = $nielson->getObject('setting', 'debug'); 
       
        if ($object->getProperty('value')->value == "On") {
            $object->getProperty('value')->value = 'Off';
        } else {
            $object->getProperty('value')->value = 'On';
        }

        $nielson->update();
    }

    //Hosts & Paths
    if (isset($_POST['btnHostAndPaths'])) {
        $object = $nielson->getObject('setting', 'HostAndPaths');
        
        $object->getProperty('Host')->value = $_POST['host'];

        $nielson->update();
    
        $returnStr = HOST . "/admin/config.php";
    }
    
    //Payment API
    if (isset($_POST['btnPaymentApi'])) {
        $object = $nielson->getObject('setting', 'PaymentAPI');
        
        $object->getProperty('MollieKey')->value = $_POST['mollie-key'];

        $nielson->update();
    }
    
    //Database
    if (isset($_POST['btnDatabase'])) {
        $object = $nielson->getObject('setting', 'Database');
        
        $object->getProperty('Host')->value = $_POST['database-host'];
        $object->getProperty('User')->value = $_POST['database-user'];
        $object->getProperty('Pass')->value = $_POST['database-pass'];
        $object->getProperty('Database')->value = $_POST['database-name'];

        $nielson->update();
    }
    
    //Mail
    if (isset($_POST['btnMail'])) {
        $object = $nielson->getObject('setting', 'Mail');
        
        $object->getProperty('Host')->value = $_POST['mail-host'];
        $object->getProperty('User')->value = $_POST['mail-user'];
        $object->getProperty('From')->value = $_POST['mail-from'];
        $object->getProperty('Pass')->value = $_POST['mail-pass'];
        $object->getProperty('Port')->value = $_POST['mail-port'];

        $nielson->update();
    }

    //Shutdown
    if (isset($_POST['btnShutdownNow'])) {
        $object = $nielson->getObject('setting', 'Shutdown');
        
        $object->getProperty('date')->value = date('Y-m-d');
        $object->getProperty('time')->value = date('H:i:s');

        $nielson->update();
        $returnStr = HOST . "/admin/shutdown.php";
    }
    if (isset($_POST['btnShutdown'])) {
        $object = $nielson->getObject('setting', 'Shutdown');
        
        $object->getProperty('date')->value = $_POST['shutdownDate'];
        $object->getProperty('time')->value = $_POST['shutdownTime'];

        $nielson->update();
        $returnStr = HOST . "/admin/shutdown.php";
    }

    //Price
    if (isset($_POST['btnPrice'])) {
        $object = $nielson->getObject('setting', 'Price');

        $object->getProperty('value')->value = $_POST['price'];

        $nielson->update();
        $returnStr = HOST . "/admin/price.php";
    }

    
    if (isset($_POST['btnUpdateTheme'])) {
        $object = $nielson->getObject('setting', 'Theme');
        
        $targetdir = "../../images/";
        
        $targetfile = $targetdir . basename($_FILES["logoFile"]["name"]);
        $image_ = ""; 
        if (isset($_FILES["logoFile"]) && move_uploaded_file($_FILES["logoFile"]["tmp_name"], $targetfile)) {
            $image_ = substr($targetfile, 6);
            $object->getProperty("logo")->value = $image_;
        }
        
        $targetfile = $targetdir . basename($_FILES["backgroundImgFile"]["name"]);
        $image_ = ""; 
        if (isset($_FILES["backgroundImgFile"]) && move_uploaded_file($_FILES["backgroundImgFile"]["tmp_name"], $targetfile)) {
            $image_ = substr($targetfile, 6);
            $object->getProperty("backgroundImg")->value = $image_;
        }
        

        $targetfile = $targetdir . basename($_FILES["faviconFile"]["name"]);
        $image_ = ""; 
        if (isset($_FILES["faviconFile"]) && move_uploaded_file($_FILES["faviconFile"]["tmp_name"], $targetfile)) {
            $image_ = substr($targetfile, 6);
            $object->getProperty("favicon")->value = $image_;
        }

        $nielson->update();
        $returnStr = HOST . "/admin/theme.php";
    }

    //Return
    header('Location: ' . $returnStr);
