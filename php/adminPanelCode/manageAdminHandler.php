<?php
    include_once('../../config/config.php');
    include_once('../classes/Database.php');
    include_once('../classes/utils.php');

    ini_set('display_errors', DEBUG);
    
    //Check if request has valid source
    if (!isset($_POST['auth'])) {
        die();
    } else {
        $file = file_get_contents(PATH . "/admin/AUTH_COOKIES/authcookie.txt.php");
        
        if (!in_array($_POST['auth'], explode(":", $file))) {
           die();
        }
    }

    $db = new mysqli(DBHOST, USER, PASS, DTBS);
    if (isset($_POST['btnAddAdmins'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $email = $_POST['email'];
        $permission = $_POST['permission'];


        foreach ($username as $key => $n) {
            $stmt = $db->prepare("INSERT INTO admins (admin, email, password, permission) VALUES (?, ?, ?, ?)");

            $stmt->bind_param("sssi", $username_, $email_, $password_, $permission_);

            $username_ = $n;
            $email_ = $email[$key];
            $password_ = password_hash($password[$key], PASSWORD_DEFAULT);
            $permission_ = $permission[$key];

            $stmt->execute();

            if ($stmt->error) {
                echo $stmt->error;
            } else {
                echo "Admins are added succesfully!";
                header('Location: ../../admin/manage-admins.php');
            }

            $stmt->close();
        }
    }

    if (isset($_POST['btnDeleteAdmin'])) {
        $stmt = $db->prepare("DELETE FROM admins WHERE ID=?");
        $stmt->bind_param("i", $ID_);
        $ID_ = $_POST['toDelete'];
        $stmt->execute();
            
        if ($stmt->error) {
            echo $stmt->error;
        } else {
            echo "Admin succesfully deleted!";
            header('Location: ../../admin/manage-admins.php');
        }
        
        $stmt->close();
    }
