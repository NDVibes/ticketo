<?php
    require_once('php/ticket_page_script.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <?php readfile(getcwd() . "/partials/head.html") ?>
        <link rel="stylesheet" type="text/css" href="style/screenshot-ticket.css">
    </head>
    <body>
        <div id="container">
            <h1>Screenshot jouw ticket!</h1>

            <div class="ticket">
                <div style="top: 0; right: 0;" class="inner-triangle"></div>
                <div class="inner-triangle" id="top-left"></div>
                <div class="inner-triangle" id="bottom-right"></div>
                <div class="inner-triangle" id="bottom-left"></div>
                
                <img src="images/BalTropical_logo_small.png" /><br>
                <span class="ad-text">NDVibes.com - Niel Duysters</span>

                <img class="qrcode" src="<?php echo htmlspecialchars(HOST."/".$path); ?>" />

                <div class="user-info">
                    <span class="firstname"><?php echo htmlspecialchars($order->users[0]->firstname); ?></span>
                    <span class="lastname"><?php echo htmlspecialchars($order->users[0]->lastname); ?></span>
                    <br><br>
                    
                    <span class="birthdate"><?php echo htmlspecialchars($order->users[0]->birthdate); ?></span><br>
                    <span class="age"><?php echo htmlspecialchars($order->users[0]->getAge()); ?></span>
                    <br><br>
                    
                    <span class="code"><?php echo htmlspecialchars($pseudocode); ?></span>

                </div>
            </div>
            <div class="next">
                <a id="btnNext" href="bedankt.php">Volgende</a>
           
                <div class="logo">
                    <img src="images/ndvibes.png" id="logo" /><br>
                    <span class="logospan">Niel Duysters</span>
                </div>
           </div>
        </div>
    </body>
</html>
