<?php
    //Read systemconfig file with Nielson
    ini_set('display_errors', 1);

    define('PATH', $_SERVER['DOCUMENT_ROOT'] . '/websites/ticketo/');
    include_once(PATH . "/php/Nielson/Nielson.php");
    
    $nielson = new Nielson(PATH . "/config/systemconfig.nielson.php");
    $nielson->read();

    //Configuration file
    $debug = false;
    if ($nielson->getObject('setting', 'debug')->getProperty("value")->value == "On")
        $debug = true;
    define('DEBUG', $debug);
    define('HOST', $nielson->getObject('setting', 'HostAndPaths')->getProperty("Host")->value);
    define('TIMEZONE', 'Europe/Brussels');

    //Include path
    set_include_path(PATH);

    //Mollie API
    define('MOLLIE_KEY', $nielson->getObject('setting', 'PaymentAPI')->getProperty("MollieKey")->value);

    //Database
    define('DBHOST', $nielson->getObject('setting', 'Database')->getProperty("Host")->value);
    define('USER', $nielson->getObject('setting', 'Database')->getProperty("User")->value);
    define('PASS', $nielson->getObject('setting', 'Database')->getProperty("Pass")->value);
    define('DTBS', $nielson->getObject('setting', 'Database')->getProperty("Database")->value);

    //Mail
    /*
    define('MAIL_USER', $nielson->getObject('setting', 'Mail')->getProperty("User")->value);
    define('MAIL_PASS', $nielson->getObject('setting', 'Mail')->getProperty("Pass")->value);
    define('MAIL_FROM', $nielson->getObject('setting', 'Mail')->getProperty("From")->value);
    define('MAIL_HOST', $nielson->getObject('setting', 'Mail')->getProperty("Host")->value);
    define('MAIL_PORT', $nielson->getObject('setting', 'Mail')->getProperty("Port")->value);
    */
    define('MAIL_USER', 'info@baltropical.be');
    define('MAIL_PASS', 'bonkiskaal');
    define('MAIL_FROM', 'info@baltropical.be');
    define('MAIL_HOST', 'mail.axc.nl');
    define('MAIL_PORT', 465);

    //Authentication
    define('AUTH', serialize(array(
        'scan' => '0593'
    )));
    
    //Price
    define('PRICE', $nielson->getObject('setting', 'Price')->getProperty("value")->value);

    //Shutdown
    $date = $nielson->getObject('setting', 'Shutdown')->getProperty("date")->value;
    $time = $nielson->getObject('setting', 'Shutdown')->getProperty("time")->value;
    define('SHUTDOWN_TIME', date($date . " " . $time));
    
/*

    //Configuration file
    define('DEBUG', true);
    define('HOST', 'https://01791795.ngrok.io/websites/ticketo');
    define('TIMEZONE', 'Europe/Brussels');

    //Include path
    define('PATH', $_SERVER['DOCUMENT_ROOT'] . "/websites/ticketo/");
    set_include_path(PATH);

    //Mollie API
    define('MOLLIE_KEY', 'test_pcTgsKs5WJWUnjj5eFzFV3RExrQKPH');

    //Database
    define('DBHOST', 'localhost');
    define('USER', 'root');
    define('PASS', 'toor');
    define('DTBS', 'TicketSystem');

    //Mail
    define('MAIL_USER', 'info@baltropical.be');
    define('MAIL_PASS', 'bonkiskaal');
    define('MAIL_FROM', 'info@baltropical.be');
    define('MAIL_HOST', 'mail.axc.nl');
    define('MAIL_PORT', 465);

    //Authentication
    define('AUTH', serialize(array(
        'scan' => '0593'
    )));

    //Price
    define('PRICE', 5.35);

    //Shutdown
    define('SHUTDOWN_TIME', '');


    //Error handling
    //register_shutdown_function( "check_for_fatal" );
    //set_error_handler( "error_handler" );
*/

    //Error handing function
    function error_handler($errno, $errstr, $errfile, $errline) {
        if (!(error_reporting() & $errno))
            return false;

        $toErrorPage = false;
        //Make error string
        $str = "(" . date('Y-m-d H:i:s') . ") ";
        switch($errno) {
            case E_USER_ERROR:
                $str .= "<b>ERROR [" . $errno . "] :</b> <span style='color=\'#FF0000\''> " . $errstr . "</span> in " . $errfile . " on line " . $errline . ". \n";
                $toErrorPage = true;
                break;

            case E_USER_WARNING:
                $str .= "<b>WARNING [" . $errno . "] :</b> <span style='color=\'#FF6600\''> " . $errstr . "</span>.\n";
                break;

            case E_USER_NOTICE:
                $str .= "<b>NOTICE [" . $errno . "] :</b> <span style='color=\'#FFFFFF\''> " . $errstr . "</span>.\n";
                break;

            default:
                $str .= "<b>Unkown Error [" . $errno . "] :</b> <span style='color=\'#FFFF00\''> " . $errstr . "</span> in " . $errfile . ".\n";
                break;
            
        }
        
        //Write to file
        $file = fopen(PATH . '/admin/systemlogs/phplog.log', 'a');
        fwrite($file, $str);
        fclose($file);
        
        if ($toErrorPage) {
            header('Location: '.HOST.'/error.php');
            exit(1);
        }
       
        return true;
    }
        
    function check_for_fatal() {
        $error = error_get_last();
        if ( $error["type"] == E_ERROR ) {
            error_handler( $error["type"], $error["message"], $error["file"], $error["line"] );
            header('Location: '.HOST.'/error.php');
            exit(1);
        }
    }
