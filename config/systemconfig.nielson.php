<?php $HIDE_NIELSON_CONTENT = ' 
[
	@setting "Debug"
	@value "On"
]
[
	@setting "HostAndPaths"
	@Host "https://b94e0160.ngrok.io/websites/ticketo"
]
[
	@setting "PaymentAPI"
	@MollieKey "test_pcTgsKs5WJWUnjj5eFzFV3RExrQKPH"
]
[
	@setting "Database"
	@Host "localhost"
	@User "root"
	@Pass "toor"
	@Database "TicketSystem"
]
[
	@setting "Mail"
	@Host "mail.axc.nl"
	@Port "465"
	@User "info@baltropical.be"
	@From "info@baltropical.be"
	@Pass "bonkiskaal"
]
[
	@setting "Shutdown"
	@date "2022-07-24"
	@time "20:42:47"
]
[
	@setting "Price"
	@value "7.35"
]
[
	@setting "Theme"
	@logo "images/bt_ticket.png"
	@favicon "images/bt_ticket.png"
	@backgroundImg "images/gijbelsstables.png"
]

';