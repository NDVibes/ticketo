<?php
    require_once('php/check_page_script.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <?php readfile(getcwd() . "/partials/head.html") ?>
        <link rel="stylesheet" type="text/css" href="style/check.css">
    </head>
    <body>
        <div id="wrapper">
            <div id="content">
                <div class="container">
                    <?php
                        //Header
                        readfile(getcwd() . "/partials/header.html");
                    ?>

                    <div class="form-wrapper">
                        <form method="post" action="betaling.php">
                            <?php
                                foreach ($users as $user) {
                                    echo 
                                        '<span class="description">Voornaam:</span>
                                        <span>' . htmlspecialchars($user->firstname) . '</span>
                                        <br>
                                        <span class="description">Achternaam:</span>
                                        <span>' . htmlspecialchars($user->lastname) . '</span>
                                        <br>
                                        <span class="description">Geboortedatum:</span>
                                        <span>' . htmlspecialchars($user->birthdate) . '</span>
                                        <br>
                                        <span class="description">E-mail:</span>
                                        <span>' .htmlspecialchars($user->email) . '</span>
                                        <br><br>'
                                    ;
                                }
                            ?>

                            <div class="form-text">
                                Klopt deze info?
                            </div>
                            
                            <input type="submit" value="Ja" name="btnNext" title="Volgende" />
                            <a href="index.php" title="Vorige">Nee</a>
                        </form>

                        <script>
                            showProgress(2,5);
                        </script>
                    </div>
                </div>
            </div>
            <?php
                readfile(getcwd() . "/partials/footer.html");
            ?>
        </div>
    </body>
</html>
