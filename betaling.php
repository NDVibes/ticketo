<?php
    require_once('php/betaling_page_script.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <?php readfile(getcwd() . "/partials/head.html") ?>
    </head>
    <body>
        <div id="wrapper">
            <div id="content">
                <div class="container">
                    <?php
                        //Header
                        readfile(getcwd() . "/partials/header.html");
                    ?>

                    <div class="form-wrapper">
                        <form method="post" action="betaling.php">
                            <div class="form-text">
                                Je hebt bijna jouw ticket! Nu enkel nog betalen
                                <div class="price">
                                    <span>Prijs: &euro;<?php echo getPrice(); ?> <span style="font-weight: light; color:#d8d8d8;">(incl. transactiekosten)</span></span>
                                </div>
                                
                                <span class="warning">
                                    Je Bancontact-app zal je na de betaling terug herleiden naar onze website, even geduld.
                                </span>
                            </div>

                            <input type="submit" value="Betalen" name="btnPay" title="Betalen" />
                        </form>

                        <script>
                            showProgress(3,5);
                        </script>
                    </div>
                </div>
            </div>
            <?php
                 readfile(getcwd() . "/partials/footer.html");
            ?>
        </div>
    </body>
</html>
